Highcharts.chart('chartColStackFin', {
    chart: {
        height : 500,
        type: 'column'
    },
    title :{
        text: null
    },
    
    exporting: { enabled: false },
    xAxis: {
        categories: annee,
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Consommation d\'énergie (en tep/an)'

        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: false,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
          color: '#ffffff',},
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y} tep/an ({point.percentage:.1f} %)<br/>Total: {point.stackTotal} tep/an'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    formatter: function () {
                        return Math.round(100 * this.y / this.total) + '%';
                 }, 
            }
        }
    },
    series: [{
        name: 'Agriculture',
        data: lsTotS1
    }, {
        name: 'Autres transports',
        data: lsTotS2
    }, {
        name: 'Déchets',
        data: lsTotS4
    },
    {
        name: 'Industrie (hors branche énergie)',
        data: lsTotS5
    }, {
        name: 'Résidentiel',
        data: lsTotS6
    }, {
        name: 'Tertiaire, commercial et institutionnel',
        data: lsTotS7
    },
    {
        name: 'Transport routier',
        data: lsTotS8
    }]
});


