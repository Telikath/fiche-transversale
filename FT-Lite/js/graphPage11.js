Highcharts.chart('chartBarZoom1', {
    chart: {
        type: 'bar'
    },
    title: {
        text: null
    },
    

    xAxis: {
        categories: ['Surface de logements'],
    },
    yAxis: {
       
        title: {
            text: '%'
        }
    },
    exporting: { enabled: false },
    
    tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
          color: '#ffffff',},
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y} ha</b> ({point.percentage:.0f}%)<br/>',
     
    },
    plotOptions: {
        bar: {
            stacking: 'percent',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    formatter: function () {
                        return Math.round(100 * this.y / this.total) + '%';
                 }, 
            }
        }
        
    },
    series: [
        {name: 'Avant 1919',
            data: [LsZoomTot[0]]
        }, 
        {name: 'de 1919 à 1945',
            data: [LsZoomTot[1]]
        },
        {name: 'de 1946 à 1970',
            data: [LsZoomTot[2]]
        }, 
        {name: 'de 1971 à 1990',
            data: [LsZoomTot[3]]
        },
        {name: 'de 1991 à 2005',
            data: [LsZoomTot[4]],
        }, 
        {name: 'Apres 2006',
            data: [LsZoomTot[5]],
        },  
]
});

Highcharts.chart('chartBarZoom2', {
    chart: {
        type: 'bar'
    },
    title: {
        text: null
    },
    

    xAxis: {
        categories: ['Surface de logements'],
    },
    yAxis: {
       
        title: {
            text: '%'
        }
    },
    exporting: { enabled: false },
    
    tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
          color: '#ffffff',},
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y} ha</b> ({point.percentage:.0f}%)<br/>',
     
    },
    plotOptions: {
        bar: {
            stacking: 'percent',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    formatter: function () {
                        return Math.round(100 * this.y / this.total) + '%';
                 }, 
            }
        }
        
    },
    series: [
        {name: 'Résidence principale',
            data: [LsZoomData[0]]
        }, 
        {name: 'Résidence secondaire',
            data: [LsZoomData[1]]
        },
        
]
});

Highcharts.chart('chartBarZoom3', {
    chart: {
        type: 'bar'
    },
    title: {
        text: null
    },
    

    xAxis: {
        categories: ['Surface de logements'],
    },
    yAxis: {
       
        title: {
            text: '%'
        }
    },
    exporting: { enabled: false },
    
    tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
          color: '#ffffff',},
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y} ha</b> ({point.percentage:.0f}%)<br/>',
     
    },
    plotOptions: {
        bar: {
            stacking: 'percent',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    formatter: function () {
                        return Math.round(100 * this.y / this.total) + '%';
                 }, 
            }
        }
        
    },
    series: [
        {name: 'Logement individuel',
            data: [LsZoomData[2]]
        }, 
        {name: 'Logement collectif',
            data: [LsZoomData[3]]
        },
        
]
});
