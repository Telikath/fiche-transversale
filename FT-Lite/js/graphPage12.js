
Highcharts.chart('chartBarZoom4', {
    chart: {
        type: 'bar'
    },
    title: {
        text: null
    },
    
    exporting: { enabled: false },
    xAxis: {
        categories: ['Avant 1919', 'De 1919 à 1945', 'De 1946 à 1970', 'De 1971 à 1990', 'De 1991 à 2005', 'Après 2006']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Consommation énergétique liée au chauffage en tep'
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }
    },
    tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
          color: '#ffffff',},
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y} Tep ({point.percentage:.1f} %)<br/>Total: {point.stackTotal} Tep'
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    formatter: function () {
                        return Math.round(100 * this.y / this.total) + '%';
                 }, 
            }
        }
    },
    series: [{
        name: lsCombZoom[0],
        data: this['lsComb'+lsCombZoom[0]]
    }, {
        name: lsCombZoom[1],
        data: this['lsComb'+lsCombZoom[1]]
    }, {
        name: lsCombZoom[2],
        data: this['lsComb'+lsCombZoom[2]]
    },{
        name: lsCombZoom[3],
        data:this['lsComb'+lsCombZoom[3]]
    }, {
        name: lsCombZoom[4],
        data: this['lsComb'+lsCombZoom[4]]
    },{
        name: lsCombZoom[5],
        data: this['lsComb'+lsCombZoom[5]]
    },]
});
