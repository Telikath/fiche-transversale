Highcharts.chart('chartPieProd', {
    chart: {
        height: 60 + '%',
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title :{
        text: null
    },
    
    tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
          color: '#ffffff',},
        pointFormat: '{point.y} GWh | {point.percentage:.1f}%</b>'
    },
    
    exporting: { enabled: false },
    plotOptions: {
        pie: {
                     
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                
                enabled: true,
                allowOverlap: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Secteur',
        colorByPoint: true,
        data: [{
            name: 'Biomasse thermique',
            y: totProd[6]
        }, {
            name: 'Biomasse electrique',
            y: totProd[1]
        }, {
            name: 'Eolien',
            y: totProd[2]
        },
        {
            name: 'Biomethane',
            y: totProd[7]
        }, {
            name: 'Solaire photovoltaique',
            y: totProd[4]
        }, {
            name: 'Solaire thermique',
            y: totProd[5]
        },
        {
            name: 'Geothermie',
            y: totProd[0]
        },
        {
            name: 'Hydraulique',
            y: totProd[3]
        }
    ]
    }]
});
