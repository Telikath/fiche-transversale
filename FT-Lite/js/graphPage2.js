

Highcharts.chart('chartGES', {
    chart: {
        //backgroundColor: '#a39999',
        type: 'bar'
    },
    title: {
      style: {
        fontSize : " 14px",
        fontWeight: 'bold',
      },
      text: 'GES ('+dataTotGes+' tonnes eq. CO2)'
    },
  
    exporting: { enabled: false },
    xAxis: {
      categories: dataSecteur,
      title: {
        text: null
      }
    },
    yAxis: {
      
      title: {
        text: '% GES',
        align: 'high'
      },
      labels: {
        overflow: 'justify'
      }
    },
    tooltip: {
      backgroundColor : 'rgba(35, 35, 35, 0.9)',
      style: {
        color: '#ffffff',},
            formatter: function(){
            
                return '<strong>'+this.x + '</strong><br/>'+dataGes[this.point.x]+'T eq CO2 | '+this.y+'%'
            }
         
    },
    plotOptions: {
      bar: {
        dataLabels: {
            formatter: function() {
                return  dataGes[this.point.x]+' T'
            }, 
          enabled: true
        }
      }
    },
    
    series: [
        {colorByPoint: true,
            data: dataStatGes,
            showInLegend: false
          }]
      });
   
        
    
