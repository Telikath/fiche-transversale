        Highcharts.chart('chartPieCarbone', {
            chart: {
                height: 45 + '%',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
        
           title:{
                text : null,
           },
            tooltip: {
                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                  style: {
                  color: '#ffffff',},
                pointFormat: '{point.y} ha | {point.percentage:.1f}%</b>'
            },
            
            exporting: { enabled: false },
            plotOptions: { 
                pie: {
                             
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Secteur',
                colorByPoint: true,
                data: [{
                    name: 'Hectares assimilés à l\'accroissement forestier',
                    y: totSurf[0]
                }, {
                    name: 'Hectares défrichés',
                    y: totSurf[1]
                }, {
                    name: 'Hectares récoltés (industrie bois)',
                    y: totSurf[2]
                },
                {
                    name: 'Hectares assimilés au changement d\'utilisation des terres',
                    y: totSurf[3]
                },
            ]
            }]
        });



var dataLab = ['Neutralité carbone','Emissions de GES des grands secteur économiques','Séquestration nette','Accroissement forestier','Récolte du bois', 'Defrichement','Changement d\'utilisation des terres'];
var dataVal = [TotDif,TotGES,Carbone[4],Carbone[0],Carbone[2],Carbone[1],Carbone[3]];      
Highcharts.chart('chartBarCarbone', {
            chart: {
                type: 'bar'
            },
            title :{
                text: null
            },
           
            exporting: { enabled: false },
            labels: {
                overflow: 'justify'
              },
            xAxis: {
                categories: dataLab,
                title: {
                  text: null
                },
            },
            yAxis: {
                
                title: {
                    text: ''
        
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            
        
            tooltip: {
                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                  style: {
                  color: '#ffffff',},
                  formatter: function(){
                    return '<strong>'+this.x + '</strong><br/>'+this.y+' T eq CO2'
                }
            },
            plotOptions: {
                bar: {
                dataLabels: {
                    formatter: function() {
                        return  this.y+' T'
                    }, 
                enabled: true
                }
            }
            },
            series: [
                {colorByPoint: true,
                    data: dataVal,
                    showInLegend: false
                  },
                 
        ],
        });
