          Highcharts.chart('chartColStackCarbone', {
            chart: {
                height: 40 + '%',
                type: 'column'
            },
            title :{
                text: null
            },
           
            exporting: { enabled: false },
            xAxis: {
                categories: annee,
                title: {
                    text: "Année"
                }
            },
            yAxis: {
                
                title: {
                    text: 'T eq CO2'
        
                },
                stackLabels: {
                    enabled: false,
                   
                }
            },
            
            legend: {
                    
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                  style: {
                  color: '#ffffff',},
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y} T eq CO2 ({point.percentage:.1f} %)<br/>Total: {point.stackTotal} T eq CO2'
            },
            plotOptions: {
                column:{
                    stacking : 'normal',
                    
                },
               
            },
            series: [{
                color: '#f7a35c',
                name: 'Accroissement forestier',
                data: Carbone1
            }, {
                color: '#8085e9',
                name: 'Récolte du bois',
                data: Carbone16
            }, {
                color: '#f15c80',
                name: 'Defrichement',
                data: Carbone14
            },
            {
                color: '#e4d354',
                name: 'Changement d\'utilisation des terres',
                data: Carbone18
            }, 
        ]
        });

        Highcharts.chart('chartEvoCarbone', {
            chart :{
                height: 35 + '%',
            },
           
            title: {
              text: null
            },
          
           
            exporting: { enabled: false },
            plotOptions: {
                series: {
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                      },
                      label: {
                        enabled: false,
                    },
                    },
                    
              },
            xAxis: {
                categories: annee,
                title: {
                    text: 'Année'
                }
            },
            yAxis: {
              title: {
                text: 'T eq CO2'
              }
            },
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            },
        
            
            series: [
                {
                    name: 'Neutralité carbone',
                    data: TotDif
                },
            {
                name: 'Emissions de GES des grands secteur économiques',
                data: TotGES
            },
            {
                name: 'Séquestration nette',
                data: Carbone20
            },
            
        ],
        tooltip: {
          backgroundColor : 'rgba(35, 35, 35, 0.9)',
           style: {
            color: '#ffffff',},
            valueSuffix: ' T eq CO2',
                shared: true,
                crosshairs: true
            
        },
          
            responsive: {
              rules: [{
                condition: {
                  maxWidth: 500
                },
                chartOptions: {
                  legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                  }
                }
              }]
            }
          
          });   
