Highcharts.chart('chartNOx', {
    chart: {
        //backgroundColor: '#a39999',
        type: 'bar'
    },
    title: {
      style: {
        fontSize : " 14px",
        fontWeight: 'bold',
      },
      text: 'NOx <br/>('+dataTotNOX+' tonnes)'
    },
  
    exporting: { enabled: false },
    xAxis: {
      categories: dataSecteur,
      title: {
        text: null
      },
    },
    yAxis: {
      min : 0,
      max : 100,
      title: {
        text: '% NOx',
        align: 'high'
      },
      labels: {
        overflow: 'justify',
      }
    },
    tooltip: {
      backgroundColor : 'rgba(35, 35, 35, 0.9)',
      style: {
        color: '#ffffff',},
            formatter: function(){
            
                return '<strong>'+this.x + '</strong><br/>'+dataNOX[this.point.x]+' T | '+this.y+'%'
            }
         
    },
    plotOptions: {
      bar: {
        dataLabels: {
            formatter: function() {
                return  dataNOX[this.point.x]+' T'
            }, 
          enabled: true
        }
      }
    },
    
    series: [
        {colorByPoint: true,
            data: dataStatNOX,
            showInLegend: false
          }]
      });   


      Highcharts.chart('chartPM10', {
        chart: {
            //backgroundColor: '#a39999',
            type: 'bar'
        },
        title: {
          style: {
            fontSize : " 14px",
            fontWeight: 'bold',
          },
          text: 'PM10 <br/>('+dataTotPM10+' tonnes)'
        },
      
        exporting: { enabled: false },
        xAxis: {
          categories: dataSecteur,
          title: {
            text: null
          },
          labels :{
          enabled: false
          }
        },
        yAxis: {
          min : 0,
          max : 100,
          title: {
            text: '% PM10',
            align: 'high'
          },
          labels: {
            overflow: 'justify',
          }
        },
        tooltip: {
          backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
            color: '#ffffff',},
                formatter: function(){
                
                    return '<strong>'+this.x + '</strong><br/>'+dataPM10[this.point.x]+' T | '+this.y+'%'
                }
             
        },
        plotOptions: {
          bar: {
            dataLabels: {
                formatter: function() {
                    return  dataPM10[this.point.x]+' T'
                }, 
              enabled: true
            }
          }
        },
        
        series: [
            {colorByPoint: true,
                data: dataStatPM10,
                showInLegend: false
              }]
          });  



          Highcharts.chart('chartSO2', {
            chart: {
                //backgroundColor: '#a39999',
                type: 'bar'
            },
            title: {
              style: {
                fontSize : " 14px",
                fontWeight: 'bold',
              },
              text: 'SO2 <br/>('+dataTotSO2+' tonnes)'
            },
          
            exporting: { enabled: false },
            xAxis: {
              categories: dataSecteur,
              title: {
                text: null
              },
              labels :{
              enabled: false
              }
            },
            yAxis: {
              min : 0,
              max : 100,
              title: {
                text: '% SO2',
                align: 'high'
              },
              labels: {
                overflow: 'justify',
              }
            },
            tooltip: {
              backgroundColor : 'rgba(35, 35, 35, 0.9)',
              style: {
                color: '#ffffff',},
                    formatter: function(){
                    
                        return '<strong>'+this.x + '</strong><br/>'+dataSO2[this.point.x]+' T | '+this.y+'%'
                    }
                 
            },
            plotOptions: {
              bar: {
                dataLabels: {
                    formatter: function() {
                        return  dataSO2[this.point.x]+' T'
                    }, 
                  enabled: true
                }
              }
            },
            
            series: [
                {colorByPoint: true,
                    data: dataStatSO2,
                    showInLegend: false
                  }]
              });            


              Highcharts.chart('chartC6H6', {
                chart: {
                    //backgroundColor: '#a39999',
                    type: 'bar'
                },
                title: {
                  style: {
                    fontSize : " 14px",
                    fontWeight: 'bold',
                  },
                  text: 'C6H6 <br/>('+dataTotC6H6+' kg)'
                },
              
                exporting: { enabled: false },
                xAxis: {
                  categories: dataSecteur,
                  title: {
                    text: null
                  },
                  labels :{
                  enabled: false
                  }
                },
                yAxis: {
                  min : 0,
                  max : 100,
                  title: {
                    text: '% C6H6',
                    align: 'high'
                  },
                  labels: {
                    overflow: 'justify',
                  }
                },
                tooltip: {
                  backgroundColor : 'rgba(35, 35, 35, 0.9)',
                  style: {
                    color: '#ffffff',},
                        formatter: function(){
                        
                            return '<strong>'+this.x + '</strong><br/>'+dataC6H6[this.point.x]+' kg | '+this.y+'%'
                        }
                     
                },
                plotOptions: {
                  bar: {
                    dataLabels: {
                        formatter: function() {
                            return  dataC6H6[this.point.x]+' kg'
                        }, 
                      enabled: true
                    }
                  }
                },
                
                series: [
                    {colorByPoint: true,
                        data: dataStatC6H6,
                        showInLegend: false
                      }]
                  });  

                  Highcharts.chart('chartHAP', {
                    chart: {
                        //backgroundColor: '#a39999',
                        type: 'bar'
                    },
                    title: {
                      style: {
                        fontSize : " 14px",
                        fontWeight: 'bold',
                      },
                      text: 'HAP <br/>('+dataTotHAP+' kg)'
                    },
                  
                    exporting: { enabled: false },
                    xAxis: {
                      categories: dataSecteur,
                      title: {
                        text: null
                      },
                      
                    },
                    yAxis: {
                      min : 0,
                      max : 100,
                      title: {
                        text: '% HAP',
                        align: 'high'
                      },
                      labels: {
                        overflow: 'justify',
                      }
                    },
                    tooltip: {
                      backgroundColor : 'rgba(35, 35, 35, 0.9)',
                      style: {
                        color: '#ffffff',},
                            formatter: function(){
                            
                                return '<strong>'+this.x + '</strong><br/>'+dataHAP[this.point.x]+' kg | '+this.y+'%'
                            }
                         
                    },
                    plotOptions: {
                      bar: {
                        dataLabels: {
                            formatter: function() {
                                return  dataHAP[this.point.x]+' kg'
                            }, 
                          enabled: true
                        }
                      }
                    },
                    
                    series: [
                        {colorByPoint: true,
                            data: dataStatHAP,
                            showInLegend: false
                          }]
                      });  

                      Highcharts.chart('chartPM2_5', {
                        chart: {
                            //backgroundColor: '#a39999',
                            type: 'bar'
                        },
                        title: {
                          style: {
                            fontSize : " 14px",
                            fontWeight: 'bold',
                          },
                          text: 'PM2.5 <br/>('+dataTotPM2_5+' tonnes)'
                        },
                      
                        exporting: { enabled: false },
                        xAxis: {
                          categories: dataSecteur,
                          title: {
                            text: null
                          },
                          labels :{
                          enabled: false
                          }
                        },
                        yAxis: {
                          min : 0,
                          max : 100,
                          title: {
                            text: '% PM2.5',
                            align: 'high'
                          },
                          labels: {
                            overflow: 'justify',
                          }
                        },
                        tooltip: {
                          backgroundColor : 'rgba(35, 35, 35, 0.9)',
                          style: {
                            color: '#ffffff',},
                                formatter: function(){
                                
                                    return '<strong>'+this.x + '</strong><br/>'+dataPM2_5[this.point.x]+' T | '+this.y+'%'
                                }
                             
                        },
                        plotOptions: {
                          bar: {
                            dataLabels: {
                                formatter: function() {
                                    return  dataPM2_5[this.point.x]+' T'
                                }, 
                              enabled: true
                            }
                          }
                        },
                        
                        series: [
                            {colorByPoint: true,
                                data: dataStatPM2_5,
                                showInLegend: false
                              }]
                          });  

                          Highcharts.chart('chartCOVNM', {
                            chart: {
                                //backgroundColor: '#a39999',
                                type: 'bar'
                            },
                            title: {
                              style: {
                                fontSize : " 14px",
                                fontWeight: 'bold',
                              },
                              text: 'COVNM <br/>('+dataTotCOVNM+' tonnes)'
                            },
                          
                            exporting: { enabled: false },
                            xAxis: {
                              categories: dataSecteur,
                              title: {
                                text: null
                              },
                              labels :{
                              enabled: false
                              }
                            },
                            yAxis: {
                              min : 0,
                              max : 100,
                              title: {
                                text: '% COVNM',
                                align: 'high'
                              },
                              labels: {
                                overflow: 'justify',
                              }
                            },
                            tooltip: {
                              backgroundColor : 'rgba(35, 35, 35, 0.9)',
                              style: {
                                color: '#ffffff',},
                                    formatter: function(){
                                    
                                        return '<strong>'+this.x + '</strong><br/>'+dataCOVNM[this.point.x]+' T | '+this.y+'%'
                                    }
                                 
                            },
                            plotOptions: {
                              bar: {
                                dataLabels: {
                                    formatter: function() {
                                        return  dataCOVNM[this.point.x]+' T'
                                    }, 
                                  enabled: true
                                }
                              }
                            },
                            
                            series: [
                                {colorByPoint: true,
                                    data: dataStatCOVNM,
                                    showInLegend: false
                                  }]
                              });  
                              Highcharts.chart('chartNH3', {
                                chart: {
                                    //backgroundColor: '#a39999',
                                    type: 'bar'
                                },
                                title: {
                                  style: {
                                    fontSize : " 14px",
                                    fontWeight: 'bold',
                                  },
                                  text: 'NH3 <br/>('+dataTotNH3+' tonnes)'
                                },
                              
                                exporting: { enabled: false },
                                xAxis: {
                                  categories: dataSecteur,
                                  title: {
                                    text: null
                                  },
                                  labels :{
                                  enabled: false
                                  }
                                },
                                yAxis: {
                                  min : 0,
                                  max : 100,
                                  title: {
                                    text: '% NH3',
                                    align: 'high'
                                  },
                                  labels: {
                                    overflow: 'justify',
                                  }
                                },
                                tooltip: {
                                  backgroundColor : 'rgba(35, 35, 35, 0.9)',
                                  style: {
                                    color: '#ffffff',},
                                        formatter: function(){
                                        
                                            return '<strong>'+this.x + '</strong><br/>'+dataNH3[this.point.x]+' T | '+this.y+'%'
                                        }
                                     
                                },
                                plotOptions: {
                                  bar: {
                                    dataLabels: {
                                        formatter: function() {
                                            return  dataNH3[this.point.x]+' T'
                                        }, 
                                      enabled: true
                                    }
                                  }
                                },
                                
                                series: [
                                    {colorByPoint: true,
                                        data: dataStatNH3,
                                        showInLegend: false
                                      }]
                                  });

