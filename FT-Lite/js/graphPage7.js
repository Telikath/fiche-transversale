
Highcharts.chart('chartEvoPES', {

    title: {
      text: 'Evolution des émissions des PES'
    },
  
  
    exporting: { enabled: false },
    plotOptions: {
      series: {
          marker: {
              enabled: false,
              symbol: 'circle',
              states: {
                  hover: {
                      enabled: true
                  }
              }
            },
            label: {
              enabled: false,
          },
          },
          
    },
    xAxis: {
        categories: annee,
        title: {
            text: 'Année'
        }
    },
    yAxis: {
      title: {
        text: 'Base 100 en 2008'
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    
    series: [{
        name: "NOx",
        data: dataEvoNOX
    },
    {
        name: "PM10",
        data: dataEvoPM10
    },
    {
        name: "SO2",
        data: dataEvoSO2
    },
    {
        name: "C6H6",
        data: dataEvoC6H6
    },
    {
        name: "HAP",
        data: dataEvoHAP
    },
    {
        name: "PM2.5",
        data: dataEvoPM2_5
    },
    {
        name: "COVNM",
        data: dataEvoCOVNM
    },
    {
        name: "NH3",
        data: dataEvoNH3
    }
],
tooltip: {
  backgroundColor : 'rgba(35, 35, 35, 0.9)',
   style: {
    color: '#ffffff',},
    valueSuffix: ' %',
        shared: true,
        crosshairs: true
    
},
  
    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  
  });   
