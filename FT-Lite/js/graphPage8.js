
  Highcharts.chart('chartIndic1', {
    chart: {
        //backgroundColor: '#a39999',
        type: 'column'
    },
    title: {
      style: {
        fontSize : " 14px",
        fontWeight: 'bold',
      },
      text: 'Valeurs maximales de la moyenne annuelle en NO2 sur le territoire'
    },
  
    exporting: { enabled: false },
    xAxis: {
      categories: anneeBis,
      title: {
        text: null
      }
    },
    yAxis: {
      max:45,
      title:{
        text : 'Concentrations (en µg/m3)'
      },
      labels: {
        overflow: 'justify'
      },
      plotLines: [{
        label :{text: 'VL=40 µg/m3',},
        
        color: 'red', // Color value
        value: 40, // Value of where the line will appear
        width: 2, // Width of the line  
                        zIndex: 5,     
      }]
    },
    tooltip: {
      backgroundColor : 'rgba(35, 35, 35, 0.9)',
      style: {
        color: '#ffffff',},
            formatter: function(){
            
                return '<strong>'+this.x + '</strong><br/>'+this.y+' µg/m3'
            }
         
    },
    plotOptions: {
      column: {
        dataLabels: {
            formatter: function() {
                return  this.y
            }, 
          enabled: true
        }
      }
    },
    
    series: [
        {colorByPoint: true,
            data: [Comm2013[0],Comm2014[0],Comm2015[0],Comm2016[0],Comm2017[0],Comm2018[0]],
            showInLegend: false
          }]
      });

      Highcharts.chart('chartIndic2', {
        chart: {
            //backgroundColor: '#a39999',
            type: 'column'
        },
        title: {
          style: {
            fontSize : " 14px",
            fontWeight: 'bold',
          },
          text: 'Valeurs maximales de la moyenne annuelle en PM10 sur le territoire'
        },
      
        exporting: { enabled: false },
        xAxis: {
          categories: anneeBis,
          title: {
            text: null
          }
        },
        yAxis: {
          max:45,
          title:{
            text : 'Concentrations (en µg/m3)'
          },
          labels: {
            overflow: 'justify'
          },
          plotLines: [{
            label :{text: 'VL=40 µg/m3',},
            
            color: 'red', // Color value
            value: 40, // Value of where the line will appear
            width: 2, // Width of the line  
                        zIndex: 5,   
          }]
        },
        tooltip: {
          backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
            color: '#ffffff',},
                formatter: function(){
                
                    return '<strong>'+this.x + '</strong><br/>'+this.y+' µg/m3'
                }
             
        },
        plotOptions: {
          column: {
            dataLabels: {
                formatter: function() {
                    return  this.y
                }, 
              enabled: true
            }
          }
        },
        
        series: [
            {colorByPoint: true,
                data: [Comm2013[1],Comm2014[1],Comm2015[1],Comm2016[1],Comm2017[1],Comm2018[1]],
                showInLegend: false
              }]
          });

          Highcharts.chart('chartIndic3', {
            chart: {
                //backgroundColor: '#a39999',
                type: 'column'
            },
            title: {
              style: {
                fontSize : " 14px",
                fontWeight: 'bold',
              },
              text: 'Nombre maximal de jours dépassant 50 µg/m3 en PM10 sur le territoire'
            },
          
            exporting: { enabled: false },
            xAxis: {
              categories: anneeBis,
              title: {
                text: null
              }
            },
            yAxis: {
              max:40,
              title:{
                text : 'Nombre de jours de dépassement'
              },
              labels: {
                overflow: 'justify'
              },
              plotLines: [{
                label :{text: 'VL=35 jours/an',},
                
                color: 'red', // Color value
                value: 35, // Value of where the line will appear
                width: 2, // Width of the line  
                        zIndex: 5,   
              }]
            },
            tooltip: {
              backgroundColor : 'rgba(35, 35, 35, 0.9)',
              style: {
                color: '#ffffff',},
                    formatter: function(){
                    
                        return '<strong>'+this.x + '</strong><br/>'+this.y+' jours/an'
                    }
                 
            },
            plotOptions: {
              column: {
                dataLabels: {
                    formatter: function() {
                        return  this.y
                    }, 
                  enabled: true
                }
              }
            },
            
            series: [
                {colorByPoint: true,
                    data: [Comm2013[2],Comm2014[2],Comm2015[2],Comm2016[2],Comm2017[2],Comm2018[2]],
                    showInLegend: false
                  }]
              });

              Highcharts.chart('chartIndic4', {
                chart: {
                    //backgroundColor: '#a39999',
                    type: 'column'
                },
                title: {
                  style: {
                    fontSize : " 14px",
                    fontWeight: 'bold',
                  },
                  text: 'Nombre maximal de jours dépassant 120 µg/m3 en O3 (en moyenne sur 8h et sur 3 ans) sur le territoire'
                },
              
                exporting: { enabled: false },
                xAxis: {
                  categories: anneeBis,
                  title: {
                    text: null
                  }
                },
                yAxis: {
                  max:30,
                  title:{
                    text : 'Nombre de jours de dépassement'
                  },
                  labels: {
                    overflow: 'justify'
                  },
                  plotLines: [{
                    label :{text: 'VC=25 jours/an',},
                    
                    color: 'red', // Color value
                    value: 25, // Value of where the line will appear
                    width: 2, // Width of the line  
                        zIndex: 5,    
                  }]
                },
                tooltip: {
                  backgroundColor : 'rgba(35, 35, 35, 0.9)',
                  style: {
                    color: '#ffffff',},
                        formatter: function(){
                        
                            return '<strong>'+this.x + '</strong><br/>'+this.y+' jours/an'
                        }
                     
                },
                plotOptions: {
                  column: {
                    dataLabels: {
                        formatter: function() {
                            return  this.y
                        }, 
                      enabled: true
                    }
                  }
                },
                
                series: [
                    {colorByPoint: true,
                        data: [Comm2013[3],Comm2014[3],Comm2015[3],Comm2016[3],Comm2017[3],Comm2018[3]],
                        showInLegend: false
                      }]
                  });

                  Highcharts.chart('chartIndic5', {
                    chart: {
                        //backgroundColor: '#a39999',
                        type: 'column'
                    },
                    title: {
                      style: {
                        fontSize : " 14px",
                        fontWeight: 'bold',
                      },
                      text: 'Valeur maximale de l\'AOT40 en O3 sur le territoire'
                    },
                  
                    exporting: { enabled: false },
                    xAxis: {
                      categories: anneeBis,
                      title: {
                        text: null
                      }
                    },
                    yAxis: {
                      
                      title:{
                        text : 'en µg/m3.h '
                      },
                      labels: {
                        overflow: 'justify'
                      },
                      plotLines: [{
                        label :{text: 'Objectif=6000 µg/m3.h',
                        x:0,
                        y:-195,
                      },
                        
                        color: 'red', // Color value
                        value: 6000, // Value of where the line will appear
                        width: 2, // Width of the line  
                        zIndex: 5,  
                        
                      }]
                    },
                    tooltip: {
                      backgroundColor : 'rgba(35, 35, 35, 0.9)',
                      style: {
                        color: '#ffffff',},
                            formatter: function(){
                            
                                return '<strong>'+this.x + '</strong><br/>'+this.y+' µg/m3.h'
                            }
                         
                    },
                    plotOptions: {
                      column: {
                        dataLabels: {
                            formatter: function() {
                                return  this.y
                            }, 
                          enabled: true
                        }
                      }
                    },
                    
                    series: [
                        {colorByPoint: true,
                            data: [Comm2013[4],Comm2014[4],Comm2015[4],Comm2016[4],Comm2017[4],Comm2018[4]],
                            showInLegend: false
                          }]
                      });

   
 