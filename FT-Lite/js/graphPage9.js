Highcharts.chart('chartBarFin', {
    chart: {
        type: 'bar'
    },
    title: {
        text: null
    },
    
    
    xAxis: {
        categories: dataSecteur,
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }
    },
    
    yAxis: {
        min: 0,
        title: {
            text: 'Base 100'
        },
        
    },
    exporting: { enabled: false },
    
    tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
          color: '#ffffff',},
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y} tep</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        bar: {
            stacking: 'percent',
           
        }
        
    },
    series: [{
        name: 'Gaz Naturel',
        data: totSectType1
    },
    {
        name: 'Produits pétroliers',
        data: totSectType2
    },
    {
        name: 'Bois-énergie (EnR)',
        data: totSectType3
    },
    {
        name: 'Autres énergies renouvelables (EnR)',
        data: totSectType4
    },
    {
        name: 'Autres non renouvelables',
        data: totSectType5
    },
    {
        name: 'Chaleur et froid issus de réseau (émissions indirectes)',
        data: totSectType7
    },
    {
        name: 'Electricité (émissions indirectes)',
        data: totSectType8
    },
    {
        name: 'Combustibles Minéraux Solides (CMS)',
        data: totSectType9
    },
    
],
});


Highcharts.chart('chartPieFin', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title :{
        text: null
    },
    
    tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
          color: '#ffffff',},
        pointFormat: '{point.y} tep | {point.percentage:.1f}%</b>'
    },
    exporting: { enabled: false },
    plotOptions: {
        pie: {
            size : 125,
           
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                alignTo: 'plotEdges',
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    fontSize: '10px'
                }
            }
        }
    },
    series: [{
        name: 'Secteur',
        colorByPoint: true,
        data: [{
            name: dataSecteur[0],
            y: lsTotS[0]
        }, {
            name: dataSecteur[1],
            y: lsTotS[1]
        }, {
            name: dataSecteur[2],
            y: lsTotS[2]
        }, {
            name: dataSecteur[3],
            y: lsTotS[3]
        }, {
            name: dataSecteur[4],
            y: lsTotS[4]
        }, {
            name: dataSecteur[5],
            y: lsTotS[5]
        }, {
            name: dataSecteur[6],
            y: lsTotS[6]
        }]
    }]
});

Highcharts.chart('chartLineFin', {

    title: {
      text: null
    },
  
    
    exporting: { enabled: false },
    plotOptions: {
        series: {
            marker: {
                enabled: false,
                symbol: 'circle',
                states: {
                    hover: {
                        enabled: true
                    }
                }
              },
              label: {
                enabled: false,
            },
            },
            
      },
    xAxis: {
        categories: annee,
        title: {
            text: 'Année'
        }
    },
    yAxis: {
      title: {
        text: 'Base 100 en 2008'
      }
    },
    legend: {
        layout: 'horizontal',
        align: 'center',
        verticalAlign: 'bottom'
      },

    
    series: [{
        name: 'Gaz Naturel',
        data: totComb1
    },
    {
        name: 'Produits pétroliers',
        data: totComb2
    },
    {
        name: 'Bois-énergie (EnR)',
        data: totComb3
    },
    {
        name: 'Autres énergies renouvelables (EnR)',
        data: totComb4
    },
    {
        name: 'Autres non renouvelables',
        data: totComb5
    },
    {
        name: 'Chaleur et froid issus de réseau (émissions indirectes)',
        data: totComb7
    },
    {
        name: 'Electricité (émissions indirectes)',
        data: totComb8
    },
    {
        name: 'Combustibles Minéraux Solides (CMS)',
        data: totComb9
    },
    
],
tooltip: {
  backgroundColor : 'rgba(35, 35, 35, 0.9)',
   style: {
    color: '#ffffff',},
    valueSuffix: ' %',
        shared: true,
        crosshairs: true
    
},
  
    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  
  });   