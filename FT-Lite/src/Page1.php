<?php


$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );

function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}

if ($_GET['Zone'] == 'EPCI'){
    $² = 2;
    $reqname = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = "Territoire de : ";
    $reqname = donnee($monPdo, $reqname);
    $name = $name.$reqname[0][0];
    $id = $_GET['ZoneBis'];
    $req="SELECT sum(sup_km2), sum(nbre_lgts), count(distinct code_insee ), sum(pop_insee) from odace.commune where code_epci ='".$_GET['ZoneBis']."'";
}
else if ($_GET['Zone'] == 'Dep'){
    $² = 1;
    $reqname = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = "Territoire du Département : ";
    $reqname = donnee($monPdo, $reqname);
    $name = $name.$reqname[0][0];
    $id = $_GET['ZoneBis'];
    $req="SELECT sum(sup_km2), sum(nbre_lgts), count(distinct code_insee ), sum(pop_insee) from odace.commune where code_dep ='".$_GET['ZoneBis']."'";
}
else if ($_GET['Zone'] == 'SCOT'){
            $² = 3;
            $reqname = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
            $name = "Territoire de : ";
            $reqname = donnee($monPdo, $reqname);
            $name = $name.$reqname[0][0];
            $id = $_GET['ZoneBis'];
            $req="SELECT sum(superficie), sum(nbre_lgts), count(distinct insee_com ), sum(population) from referentiel_geo.com_scot_2019 a inner join odace.commune b on a.insee_com = b.code_insee where id_scot ='".$_GET['ZoneBis']."'";
}
else {
    $² = 0;
    $name = "Territoire de la région Centre-Val de Loire";
    $id = "R24";
    $req="SELECT sum(sup_km2), sum(nbre_lgts), count(distinct code_insee ), sum(pop_insee) from odace.commune ";
}
    $result = donnee($monPdo, $req);
    if($_GET['Zone'] == 'SCOT'){
        $surface = round($result[0][0]/1000000);
    }
    else {
        $surface = round($result[0][0]);
    }
        $nb_lgts = round($result[0][1]);
        $nb_com = round($result[0][2]);
        $nb_pop = round($result[0][3]);

$req="SELECT sum(nbre_lgts), sum(pop_insee) from odace.commune ";
$result = donnee($monPdo, $req);
    $nb_lgtsReg = round($result[0][0]);
    $nb_popReg = round($result[0][1]);

    
    
?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/graph.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie0.css">

    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
            <div class='tBan'>
                <h3>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h3>
                <h4><?php echo $name?></h4>
            </div>  
        </div>
        <div class="gbl">
            <div class="donnee">
                <div>
                    <p class="texte">
                    Cette ﬁche territoriale synthétise les principales informations relatives aux émissions de Gaz à Eﬀet de Serre (GES), 
                    à la qualité de l’air, à la séquestration carbone, à la consommation d’énergie et à la production d’énergie renouvelable à l’échelle <?php if ($²==0){echo ' de la région';} 
                                                                                                                                                                else if ($²==1){echo ' du département' ;}
                                                                                                                                                                else if ($² == 2){echo ' de l’Établissement Public de Coopération Intercommunal (EPCI)';} 
                                                                                                                                                                else { echo ' Schéma de Cohérence Territoriale (SCoT)';}?>.
                    Ces informations sont principalement issues de l’invenTaire Régional Air-Climat-Energie (TRACE) de Lig’Air pour l’année de référence 2016. 
                    À la ﬁn de ce document sont fournis des informations et des liens vers des données complémentaires à cette ﬁche synthétique.
                    </p>
                </div>
                <div class="gbCarte shadow-sm">
                    <div class="sTitre">
                        <h5>CARTE D’IDENTITÉ DU TERRITOIRE</h5>
                    </div>
                    <div class="carte">
                        <div class="sCarte">
                       
                            <div>
                                <img <?php echo "src='../../include/".$id."/carte_situation_2.png'"?>alt=""  style='width : 99%; max-width: 250px; max-height:300px'>
                            </div>
                            <div>
                                <img <?php echo "src='../../include/".$id."/LEGENDE_population_2.png'"?> alt="" style='width : 99%; max-width: 250px; max-height:150px'>
                            </div>
                        </div>
                        <div>
                            <img  <?php echo"src='../../include/".$id."/CARTE_population.png'"?> alt=""  style='width : 99%; max-width: 400px; max-height:450px'>
                        </div>
                       
                    </div>
                </div>
                <div class="table">
                    <table class="thead-dark shadow-sm">
                        <thead>
                            <tr>
                                <th>Nombre d'habitants</th>
                                <th>% de la population régionale</th>
                                <th>Superficie (en km²)</th>
                                <th>Nombre de communes</th>
                                <th>Nombre de logements</th>
                                <th>% des logements régionaux</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $nb_pop?></td>
                                <td><?php echo round(100*$nb_pop/$nb_popReg)?></td>
                                <td><?php echo $surface?></td>
                                <td><?php echo $nb_com?></td>
                                <td><?php echo $nb_lgts?></td>
                                <td><?php echo round(100*$nb_lgts/$nb_lgtsReg) ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div> 
            </div>
            
        </div>
    
        <footer>
        <center>- 1 -</center>
    </footer>
    </body>
</html>