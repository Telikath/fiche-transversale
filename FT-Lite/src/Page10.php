<?php
$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );


function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}
if ($_GET['Zone'] == 'EPCI'){
    $name = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="SELECT SUM(valeur) as val, a.id_ssecteur as id, b.lib_stype as name, annee
        FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type  inner join odace.commune c ON a.insee_com = c.code_insee
        where a.id_secteur = 2 and a.id_ssecteur!=3 and a.id_scombust != 10 and code_epci ='".$_GET['ZoneBis']."'
        group by b.lib_stype,a.id_ssecteur, annee
        order by a.id_ssecteur, annee";
}
else if ($_GET['Zone'] == 'Dep'){
    $name = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="SELECT SUM(valeur) as val, a.id_ssecteur as id, b.lib_stype as name, annee
        FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type  inner join odace.commune c ON a.insee_com = c.code_insee
        where a.id_secteur = 2 and a.id_ssecteur!=3 and a.id_scombust != 10 and code_dep ='".$_GET['ZoneBis']."'
        group by b.lib_stype,a.id_ssecteur, annee
        order by a.id_ssecteur, annee";
}
else if ($_GET['Zone'] == 'SCOT'){
    $name = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];    
    $req="SELECT SUM(valeur) as val, a.id_ssecteur as id, b.lib_stype as name, annee
    FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type  inner join referentiel_geo.com_scot_2019 c on a.insee_com = c.insee_com
    where a.id_secteur = 2 and a.id_ssecteur!=3 and a.id_scombust != 10 and id_scot ='".$_GET['ZoneBis']."'
    group by b.lib_stype,a.id_ssecteur, annee
    order by a.id_ssecteur, annee";
    
}
else {
    $name = "Centre-Val de Loire";
    $req="SELECT SUM(valeur) as val, a.id_ssecteur as id, b.lib_stype as name, annee
        FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type  inner join odace.commune c ON a.insee_com = c.code_insee
        where a.id_secteur = 2 and a.id_ssecteur!=3 and a.id_scombust != 10 
        group by b.lib_stype,a.id_ssecteur, annee
        order by a.id_ssecteur, annee";
}

$LsSect = [];

   
    $annee = [];      
    
    $result = donnee($monPdo, $req);
            $i = 0;
            foreach($result as $row){
                if ($i != $row['id']){
                    $i = $row['id'];
                    ${'lsTotS'.$i} = [];
                    array_push(${'lsTotS'.$i}, round($row[0],0,PHP_ROUND_HALF_UP));
                }
                else {
                    array_push(${'lsTotS'.$i}, round($row[0],0,PHP_ROUND_HALF_UP));
                }
                if (!in_array($row['annee'], $annee)){
                    array_push($annee, $row['annee']);
                }
                if (!in_array($row['name'], $LsSect)){
                    array_push($LsSect, $row['name']);
                }
            }
            echo '<script type="text/javascript">3
                var lsTotS1 = '.json_encode($lsTotS1).';
                var lsTotS2 = '.json_encode($lsTotS2).';
                var lsTotS4 = '.json_encode($lsTotS4).';
                var lsTotS5 = '.json_encode($lsTotS5).';
                var lsTotS6 = '.json_encode($lsTotS6).';
                var lsTotS7 = '.json_encode($lsTotS7).';
                var lsTotS8 = '.json_encode($lsTotS8).';
                var dataSecteur = '.json_encode($LsSect).';  
                var annee = '.json_encode($annee).';  
            </script>';


            
?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie4.css">

    
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        
    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
            <div class='tBan'>
                <h3>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h3>
                <h4><?php echo $name?></h4>
                <h4>Consommation d’énergie ﬁnale</h4>
            </div>          
        </div>
            <div class="princBlock margin shadow-sm" style="margin-bottom: 1%">
                <div class='sTitre'>
                    <h5>Evolution de la consommation d’energie ﬁnale par secteurs entre 2008 et 2016</h5>
                </div>
                <div id="chartColStackFin" class="padding"></div>
            </div>
            <div class="methodo shadow-sm">
                <div class="sTMeth">
                    <h5>METHODOLOGIE</h5>
                </div>
                <div class="meth">
                    <p>La donnée de consommation d’énergie ﬁnale est issue du travail d’inventaire des émissions de polluants atmosphériques réalisé par Lig’Air. Conformément au PCAET, « la consommation énergétique ﬁnale est ainsi la consommation de toutes les branches de l’économie, à l’exception des quantités consommées par les producteurs et transformateurs d’énergie (exemple : consommation propre d’une raﬃnerie) et des quantités de produits énergétiques transformés en d’autres produits ». Concernant le secteur des déchets et conformément à cette déﬁnition seules sont prises en compte dans l’Atlas les consommations des unités de traitement qui ne valorisent pas l’énergie. Autrement dit Les consommations du secteur Branche énergie ne sont pas comptabilisées. Aﬁn de prendre en compte l’ensemble des énergies, l’électricité et la chaleur sont ajoutées aux combustibles (utilisés à des ﬁns de consommation énergétique) évalués dans l’inventaire des émissions. Les données sont fournies à climat réel, c’est-à-dire qu’elles ne sont pas corrigées des variations climatiques et le pouvoir caloriﬁque inférieur pour les combustibles est retenu conformément à l’arrêté PCAET.</p>
                    <p>La tonne d’équivalent pétrole (tep) est une unité de mesure couramment utilisée pour comparer les diﬀérentes énergies entre-elles. C’est l’énergie produite par la combustion d’une tonne de pétrole moyen (1 tep=11,6 MWh). Pour plus de précisions, vous pouvez consulter les notes méthodologiques et/ou contacter l’OREGES : oreges@ligair.fr.</p>
                </div>
            </div>
        
        <script src="../js/graphPage10.js"></script>
        <footer>
        <center>- 10 -</center>
    </footer>
    </body>
</html>