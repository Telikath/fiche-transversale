<?php
$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );

function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}
if ($_GET['Zone'] == 'EPCI'){
    $name = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    
}
else if ($_GET['Zone'] == 'Dep'){
    $name = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
   
}
else if ($_GET['Zone'] == 'SCOT'){
    $name = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
   
}
else {
    $name = "Centre-Val de Loire";
    
}

?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie7.css">

        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        
    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
            <div class='tBan'>
                <h3>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h3>
                <h4><?php echo $name?></h4>
                <h4>Compléments d’information et liens utiles</h4>
            </div>   
        </div>
        <div class="princBlock margin shadow-sm" >
            <div class='sTitre'>
                <h5>Documents de reférence</h5>
            </div>
            <div class="texte margin">
                <p>
                    <b>Informations contextuelles (Rappel des enjeux, éléments de déﬁnitions, (PES, GES par exemple), ...) :</b>
                    <ul>
                        <li>
                            <a href="https://www.ligair.fr/la-pollution/leﬀet-de-serre">https://www.ligair.fr/la-pollution/leﬀet-de-serre</a>
                        </li>
                        <li>
                            <a href="http://www.observatoire-energies-centre.org/donnees-territoires/bilan-regional/emissions-ges.html">http://www.observatoire-energies-centre.org/donnees-territoires/bilan-regional/emissions-ges.html</a>
                        </li>
                    </ul>
                </p>
                <p>    
                    <b>Enquête sur le chauﬀage bois domestique en région Centre-Val de Loire : </b>
                    <ul>
                        <li>
                            <a href="http://www.observatoire-energies-centre.org/les-energies-renouvelables/energies-renouvelables/biomasse/bois-energie.html">http://www.observatoire-energies-centre.org/les-energies-renouvelables/energies-renouvelables/biomasse/bois-energie.html</a>
                        </li>
                        <li>
                            <a href="https://www.ademe.fr/etude-chauﬀage-domestique-bois">https://www.ademe.fr/etude-chauﬀage-domestique-bois</a> 
                        </li>
                    </ul>
                </p>
                <p>
                    <b>Objectifs régionaux SRCAE / SRADDET ou nationaux : </b>
                    <ul>
                        <li>
                            <a href="https://www.democratie-permanente.fr/project/centre-val-de-loire-la-region">https://www.democratie-permanente.fr/project/centre-val-de-loire-la-region</a> 
                        </li>
                        <li>
                            <a href="http://www.observatoire-energies-centre.org/donnees-territoires/planiﬁcation-energetique.html">http://www.observatoire-energies-centre.org/donnees-territoires/planiﬁcation-energetique.html </a>
                        </li>
                    </ul>
                </p>
                <p>
                    <b>Infrastructures routières et départementales :</b> <a href="http://www.ort-centre.fr/Routieres">http://www.ort-centre.fr/Routieres</a></br>
                    <b>Immatriculations des véhicules particuliers neufs :</b> <a href="http://www.ort-centre.fr/Immatriculations">http://www.ort-centre.fr/Immatriculations</a></br>
                    <b>Motifs de déplacement :</b> <a href="http://www.ort-centre.fr/Deplacements-de-Voyageurs">http://www.ort-centre.fr/Deplacements-de-Voyageurs</a></br>
                    <b>Parc départemental par type de véhicules :</b> <a href="http://www.ort-centre.fr/Parcs-de-vehicules">http://www.ort-centre.fr/Parcs-de-vehicules</a></br>
                    <b>Enquêtes Nationales Transports Déplacement :</b> <a href="http://www.ort-centre.fr/Une-enquete-nationale-transports?rech=1">http://www.ort-centre.fr/Une-enquete-nationale-transports?rech=1</a></br>
                    <b>Transport de marchandises :</b> <a href="http://www.ort-centre.fr/Flux-inter-regionaux-de-47">http://www.ort-centre.fr/Flux-inter-regionaux-de-47</a>
                </p>
                <p>
                    <b>Consommation énergétique des collectivités :</b> <a href="https://www.ademe.fr/expertises/batiment/chiﬀres-cles-observations/lenquete-energie-patrimoine-communal">https://www.ademe.fr/expertises/batiment/chiﬀres-cles-observations/lenquete-energie-patrimoine-communal</a>
                </p>
                <p>
                    <b>Enquête OPEN (observatoire de l’amélioration énergétique du logement) :</b> <a href="https://www.ademe.fr/open-observatoire-permanent-lamelioration-energetique-logement-campagne-2015">https://www.ademe.fr/open-observatoire-permanent-lamelioration-energetique-logement-campagne-2015</a> 
               
                </p>
                <p>
                    <b>Outil Aldo de l’ADEME pour l’estimation de la séquestration carbone (approche complémentaire) : </b> <a href="https://www.territoires-climat.ademe.fr/ressource/211-76">https://www.territoires-climat.ademe.fr/ressource/211-76</a>
                    
                </p>
                <p>
                    <b>« Guide méthodologique pour l’élaboration des inventaires territoriaux des émissions atmosphériques (polluants de l’air et gaz à eﬀet deserre)»,Pôle National de Coordination des Inventaires Territoriaux,édité par le Ministère de l’Ecologie, du Développement Durable et de l’Energie, novembre 2012 :</b> <a href="http://www.lcsqa.org/">http://www.lcsqa.org/</a> 
              
                </p>
                <p>
                    <b>Arrêté SNIEBA du 24 août 2011 relatif au Système National d’Inventaires d’Emissions et de Bilans dans l’Atmosphère : </b> <a href="http://www.legifrance.gouv.fr/">http://www.legifrance.gouv.fr/</a>
                </p>
                <p>
                    <b>Arrêté du 4 août 2016 relatif au Plan Climat–Air-Energie Territorial :</b>  <a href="http://www.legifrance.gouv.fr/">http://www.legifrance.gouv.fr/</a>
                    
                </p>
                <p>
                    <b>« Guide Plan Climat-Air-Energie Territorial (PCAET) », réf. 8674, publié par l’ADEME, en décembre 2016 :</b>  <a href="http://www.ademe.fr/pcaet-comprendre-construire-mettre-oeuvre">http://www.ademe.fr/pcaet-comprendre-construire-mettre-oeuvre></a>
                </p>
                <p>
                    <b>« Bilan de l’inventaire des émissions de polluants à eﬀet sanitaire et gaz à eﬀet de serre », réf. emi2012_v1.4/2017, publié par Lig’Air, en septembre 2017. Les notes méthodologiques et des résultats à l’échelle de la commune sont également disponibles. :</b> <a href="https://www.ligair.fr/les-moyens-d-evaluation/inventaire-des-emissions-1">https://www.ligair.fr/les-moyens-d-evaluation/inventaire-des-emissions-1</a> 
                </p>
                <p>
                    <b>Plans de Protection de l’atmosphère (PPA), pour les agglomérations de Tours et d’Orléans :</b> <a href="http://www.centre.developpement-durable.gouv.fr/plans-de-protection-de-l-atmosphere-r176.html">http://www.centre.developpement-durable.gouv.fr/plans-de-protection-de-l-atmosphere-r176.html</a>
            
                </p>
                <p>
                    <b>« Synthèses et cahiers cartographiques du Proﬁl Environnemental Régional » publiés par la DREAL Centre-Val de Loire : </b> <a href="http://www.centre.developpement-durable.gouv.fr/les-syntheses-et-les-cahiers-cartographiques-du-r890.html">http://www.centre.developpement-durable.gouv.fr/les-syntheses-et-les-cahiers-cartographiques-du-r890.html<a>
                    
                </p>
            </div>
        </div>   
        <footer>
        <center>- 16 -</center>
    </footer>     
    </body>
</html>