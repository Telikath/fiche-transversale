<?php
$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );

function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}
if ($_GET['Zone'] == 'EPCI'){
    $name = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "select code_insee as code, nom_com as nom
    from odace.commune where code_epci = '".$_GET['ZoneBis']."'
    order by code_insee";
    $result = donnee($monPdo, $req);
    
}
else if ($_GET['Zone'] == 'Dep'){
    $name = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
   
}
else if ($_GET['Zone'] == 'SCOT'){
    $name = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "select a.code_insee as code, a.nom_com as nom
    from odace.commune a inner join referentiel_geo.com_scot_2019 b on b.insee_com = a.code_insee where id_scot = '".$_GET['ZoneBis']."'
    order by code_insee";
    $result = donnee($monPdo, $req);
   
}
else {
    $name = "Centre-Val de Loire";
    
}
?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie7.css">

        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        
    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
            <div class='tBan'>
                <h3>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h3>
                <h4><?php echo $name?></h4>
                <h4>Compléments d’information et liens utiles</h4>
            </div>   
        </div>  
        <div class="princBlock margin shadow-sm" >
            <div class='sTitre'>
                <h5>Les partenaires</h5>
            </div>
            <div class="texte margin">
                <b>DREAL Centre-Val de Loire :</b> <a href="http://www.centre.developpement-durable.gouv.fr">http://www.centre.developpement-durable.gouv.fr</a></br>
                <b>ADEME Centre-Val de Loire :</b> <a href="http://www.centre.ademe.fr">http://www.centre.ademe.fr</a></br>
                <b>Région Centre-Val de Loire :</b> <a href="http://www.regioncentre-valdeloire.fr">http://www.regioncentre-valdeloire.fr et <a href="http://www.energies-centre.fr">http://www.energies-centre.fr</a></br>
                <b>Lig’Air (association agréée de surveillance de la qualité de l’air en région Centre-Val de Loire) :</b> <a href="http://www.ligair.fr">http://www.ligair.fr</a></br>
                <b>Oreges (Observatoire Régional de l’Energie et des GES) :</b> <a href="http://www.observatoire-energies-centre.org">http://www.observatoire-energies-centre.org</a>
            </div>
        </div>
        <div class="methodo shadow-sm ">
                <div class="sTMeth">
                    <h5>CONDITIONS D’UTILISATION</h5>
                </div>
                <div class="meth">
                <p>Les données contenues dans ce document restent à la propriété intellectuelle de Lig’Air/OREGES. Toute utilisation par-tielle ou totale de ce document (extrait de texte, graphiques, tableaux, ...) doit faire référence aux sources précisées sur la fiche dans les termes suivants : « Lig’Air/OREGES (Année de diffusion) - Fiches Territoriales ». Les données de concentrations de polluants sont mises à jour annuellement au cours du 1er semestre, afin d’intégrer les dernières données disponibles et prendre en compte l’amélioration de la chaine de modélisation. Les données d’émissions de polluants sont régulièrement mises à jour afin d’intégrer les dernières données disponibles et prendre en compte l’évolution des facteurs d’émissions liées aux améliorations des connaissances sur les rejets de polluants atmosphériques. Par ailleurs, Lig’Air/OREGES ne sont en aucune façon responsables des interprétations et travaux intellectuels, publications diverses résultant de ses travaux et pour lesquels aucun accord préalable n’aurait été donné.</p>
                </div>
            </div>
            <div class="meth princBlock shadow-sm margin" >
                <img src="../../include/BLOC_MARQUE_CPER_REGION_PREF_ADEME_VF5.jpg" alt="" style="width : 90%">
            </div>
        <?php
         if ($_GET['Zone'] == 'SCOT' || $_GET['Zone'] == 'EPCI'){
                echo "<div class='princBlock margin shadow-sm ' style='margin-bottom: 1%'>
                <div class='sTitre'>
                    <h4>Liste des communes du territoire</h4>
                </div>
                <div class='com margin'>";
                $x=0;
                foreach( $result as $row){
                    $x++;
                    echo "(".$x.") ".$row['code']." ".$row['nom']."; ";
                           
            }
            echo "</div></div>";
        }
        ?>
          
            <footer>
        <center>- 17 -</center>
    </footer>
    </body>
</html>