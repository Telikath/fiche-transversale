<?php
$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );

function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}

$LsSect = [];
$ges = [];
$totges=[];
$StatGes=[];
if ($_GET['Zone'] == 'EPCI'){
    $name = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    
    $req="(SELECT secteur, sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
    from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_epci ='".$_GET['ZoneBis']."'
                group by secteur)
            union 
            (SELECT 'Total' as secteur , sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
            from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_epci ='".$_GET['ZoneBis']."')
            order by ges desc";
}
else if ($_GET['Zone'] == 'Dep'){
    $name = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="(SELECT secteur, sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
    from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_dep ='".$_GET['ZoneBis']."'
                group by secteur)
            union 
            (SELECT 'Total' as secteur , sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
            from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_dep ='".$_GET['ZoneBis']."')
            order by ges desc";
}
else if ($_GET['Zone'] == 'SCOT'){
    $name = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="(SELECT secteur, sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
    from inventaire_emission.inventaire_pcaet a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
    WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.id_scot ='".$_GET['ZoneBis']."'
    group by secteur)
union 
(SELECT 'Total' as secteur , sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
    from inventaire_emission.inventaire_pcaet a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
    WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.id_scot ='".$_GET['ZoneBis']."')
order by ges desc";
}
else {
    $name = "Centre-Val de Loire";
    $req="(SELECT secteur, sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
                from inventaire_emission.inventaire_pcaet
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' 
                group by secteur)
            union 
            (SELECT 'Total' as secteur , sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
                from inventaire_emission.inventaire_pcaet
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus')
            order by ges desc";
}

$result = donnee($monPdo, $req);
foreach($result as $row){
    if ($row['secteur'] == 'Total'){
        array_push($totges, $row['ges']);
    }
    else{
        array_push($LsSect, $row['secteur']);
        array_push($ges, number_format(round($row['ges'],0,PHP_ROUND_HALF_UP),0,'',' '));
        array_push($StatGes, round(100*$row['ges']/$totges[0],1));
        
    }
}

echo '<script type="text/javascript">
        var dataGes = '.json_encode($ges).';   
        var dataStatGes ='.json_encode($StatGes).'; 
        var dataSecteur = '.json_encode($LsSect).';
        var dataTotGes = '.json_encode(number_format($totges[0],0,'',' ')).';
        </script>';

# calcule sur la region entiere
$reqReg="SELECT sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
            from inventaire_emission.inventaire_pcaet
            WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus'";
$resultreg = donnee($monPdo, $reqReg);


$reqSectReg="SELECT secteur, sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
                from inventaire_emission.inventaire_pcaet
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' 
                group by secteur
                order by ges desc";
$resultSectReg = donnee($monPdo, $reqSectReg);


?>



<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie1.css">

        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        
    </head>
    <body>
        <div class="shadow-sm p-0 mb-0 titre">
            <div class='tBan'>
                <h3>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h3>
                <h4><?php echo $name?></h4>
                <h4>Émissions de Gaz à Eﬀet de Serre (GES)</h4>
            </div>
        </div>
            <div class="methodo shadow-sm">
                <div class="sTMeth">
                    <h5>METHODOLOGIE</h5>
                </div>
                <div class="meth">
                    <p>L’inventaire des émissions réalisé par Lig’Air est orienté « source » ce qui signiﬁe que les émissions sont comptabilisées au niveau du territoire où elles sont produites. Ces travaux s’appuient sur des données statistiques oﬃcielles et sur le guide PCIT (Pôle de Coordination nationale des Inventaires Territoriaux). Les résultats présentés sont reportés en cohérence avec le décret et l’arrêté relatifs au PCAET. Les émissions de GES liées à la production d’électricité et de chaleur sont introduites au niveau des secteurs utilisateurs (résidentiel, tertiaire, ...). Les 7 GES retenues actuellement dans le protocole de Kyoto (CO2, CH4, N2O, HFC, PFC, SF6 et NF3 sont pris en compte à ce jour dans l’inventaire de Lig’Air. Le terme FLUORES rassemble les émissions de HFC, PFC, SF6 et NF3). Les émissions de GES sont exprimées en pouvoir de réchauﬀement global (PRG). Les valeurs de PRG utilisées sont celles du 5ième rapport du GIEC.</p><p> Pour plus de précisions, vous pouvez consulter les notes méthodologiques et/ou contacter l’Oreges : oreges@ligair.fr, ou Lig’Air : ligair@ligair.fr.</p>
                </div>
            </div>
            <div class="" >
                <div class="donnee">
                    <div>
                        <p>En 2016, les émissions de gaz à eﬀet de serre du territoire s’élèvent à <?php echo number_format($totges[0],0,',',' '); ?> tonnes équivalent CO2 (soit <?php echo round(100*round($totges[0])/round($resultreg[0]['ges']))?>% des émissions régionales de GES). Le secteur <?php echo $LsSect[0] ?> constitue le premier secteur émetteur sur le territoire, suivi par le secteur <?php echo $LsSect[1] ?> et le secteur <?php echo $LsSect[2] ?>. Au niveau régional, le principal secteur émetteur est le <?php echo $resultSectReg[0][0] ?>. </p>
                    </div>
                </div>
                <div class="princBlock shadow-sm">
                    <div class='sTitre'>
                        <h5>Contribution des secteurs aux émissions de GES</h5>
                    </div>
                        <div id="chartGES" class="padding"></div>
                </div>
            </div>  
 

        
        <script src="../js/graphPage2.js"></script>
        <footer>
        <center>- 2 -</center>
    </footer>
    </body>
</html>