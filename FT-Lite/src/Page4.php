<?php
$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );

function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}

if ($_GET['Zone'] == 'EPCI'){
    $name = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "SELECT sum(a.valeur) as val, a.id_type 
        from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee 
        where a.id_type in (2,15,19,17) and a.annee = 2016 and c.code_epci ='".$_GET['ZoneBis']."'
        group by a.id_type
        order by a.id_type";

    $req2="SELECT sum(a.valeur) as val, a.id_type from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee where a.id_type in (1,18,14,16,20) and  a.annee = 2016 and c.code_epci ='".$_GET['ZoneBis']."'
        group by a.id_type
        order by a.id_type"; 
        
    $req3="SELECT sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
        from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
        WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_epci ='".$_GET['ZoneBis']."'";
}
else if ($_GET['Zone'] == 'Dep'){
    $name = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "SELECT sum(a.valeur) as val, a.id_type 
        from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee 
        where a.id_type in (2,15,19,17) and a.annee = 2016 and code_dep ='".$_GET['ZoneBis']."'
        group by a.id_type
        order by a.id_type";

    $req2="SELECT sum(a.valeur) as val, a.id_type 
        from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee 
        where a.id_type in (1,18,14,16,20) and  a.annee = 2016 and code_dep ='".$_GET['ZoneBis']."'
        group by a.id_type
        order by a.id_type"; 
        
    $req3="SELECT sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
        from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
        WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_dep ='".$_GET['ZoneBis']."'";
}
else if ($_GET['Zone'] == 'SCOT'){
    $name = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "SELECT sum(a.valeur) as val, a.id_type 
        from odace.sequestration a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
        where a.id_type in (2,15,19,17) and a.annee = 2016 and id_scot ='".$_GET['ZoneBis']."'
        group by a.id_type
        order by a.id_type";

    $req2="SELECT sum(a.valeur) as val , a.id_type 
    from odace.sequestration a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
    where a.id_type in (1,18,14,16,20) and  a.annee = 2016 and id_scot ='".$_GET['ZoneBis']."'
    group by a.id_type
    order by a.id_type"; 
        
    $req3="SELECT sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as val
    from inventaire_emission.inventaire_pcaet a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
    WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and id_scot ='".$_GET['ZoneBis']."'";
}
else {
    $name = "Centre-Val de Loire";
    $req = "SELECT sum(a.valeur) as val, a.id_type 
        from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee 
        where a.id_type in (2,15,19,17) and a.annee = 2016
        group by a.id_type
        order by a.id_type";

    $req2="SELECT sum(a.valeur) as val, a.id_type from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee where a.id_type in (1,18,14,16,20) and  a.annee =2016
        group by a.id_type
        order by a.id_type"; 

    $req3="SELECT sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as ges	
        from inventaire_emission.inventaire_pcaet
        WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus'";      
    
}


$totCarboneSurf =[];
$result = donnee($monPdo, $req);
        foreach($result as $row){
            array_push($totCarboneSurf,round($row['val'],0,PHP_ROUND_HALF_UP));
        }       

echo '<script type="text/javascript">
var totSurf = '.json_encode($totCarboneSurf).';
</script>';  


    $Carbone = [];
           
    $result = donnee($monPdo, $req2);
        foreach($result as $row){
        array_push($Carbone,round($row['val'],0,PHP_ROUND_HALF_UP));            
                }
    
    echo '<script type="text/javascript">
        var Carbone = '.json_encode($Carbone).';
           </script>';  
      
        $result = donnee($monPdo, $req3);
        $TotGES = round($result[0][0],0,PHP_ROUND_HALF_UP); 
        $TotDif = $TotGES+$Carbone[4];

        echo '<script type="text/javascript">
        var TotGES= '.json_encode($TotGES).';
        var TotDif= '.json_encode($TotDif).';
           </script>';
           
?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie2.css">


        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        
    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
            <div class='tBan'>
                <h3>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h3>
                <h4><?php echo $name?></h4>
                <h4>Estimation de la séquestration carbone du territoire</h4>
            </div>
        </div>
        <div class="princBlock shadow-sm margin">
            <div class='sTitre'>
                <h5>Séquestration nette de carbone</h5>
            </div>
            <div><p class="texte">En 2016, la séquestration nette de carbone du territoire est estimé à <?php echo number_format($Carbone[4],0,'',' ')?> teqCO2. Pour rappel, les émissions de GES des grands secteurs économiques du territoire étaient de <?php echo number_format($TotGES,0,'',' ')?> teqCO2. La neutralité carbone <?php if ($TotDif == 0){echo ' est atteinte ';}
                                                                                                                                                                                                                                                                                                                                                                    else if ($TotDif < 0){echo 'est dépassée';}
                                                                                                                                                                                                                                                                                                                                                                    else { echo 'n’est pas atteinte';}   ?> puisqu’il résulte, de la somme de la séquestration nette et des émissions de GES des grands secteurs économiques, un bilan <?php if ($TotDif==0) { echo 'neutre de '.number_format($TotDif,0,'',' ').' teqCO2. Le territoire émet autant de CO2 que ce qu’il en séquestre.';}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            elseif ($TotDif>0){echo 'positif de '.number_format($TotDif,0,'',' ').' teqCO2. Le territoire émet plus de CO2 que ce qu’il en séquestre.';}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            else {echo 'negatif de '.number_format($TotDif,0,'',' ').' teqCO2. Le territoire émet moins de CO2 que ce qu’il en séquestre.';}?><p></div>
            <div class="blockRepart">
                <div id="chartBarCarbone" class="padding" ></div>
                    <p class="texte"><U>Séquestration nette de carbone :</U> <i>(somme des ﬂux : accroissement forestier, changement d’aﬀectation des sols, défrichement et récolte du bois)</i></p>
                    <p class="texte"><U>Evaluation de la neutralité carbone :</U> <i>(sommes des émissions GES des grands secteurs économiques et de la séquestration nette)</i></p>    
                </div> 
                <div class="blockRepart">
                    <div id="chartPieCarbone" class="padding" ></div>           
                </div>   
            </div>
        </div>      
            <script src="../js/graphPage4.js"></script>
            <footer>
        <center>- 4 -</center>
    </footer>
        </body>
    </html>