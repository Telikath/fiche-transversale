<?php
$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );


function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}

if ($_GET['Zone'] == 'EPCI'){
    $name = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "SELECT annee, SUM(nox_tonne) as nox, sum(pm10_tonne)as pm10, sum(so2_tonne) as so2, sum(benzene_kg) as c6h6, sum(hap8_kg) as hap, sum(pm2_5_tonne) as pm2_5, sum(covnm_tonne) as covnm, sum(nh3_tonne) as nh3	
                from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
                WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_epci ='".$_GET['ZoneBis']."'
            group by annee
            order by annee ";
    $req2="SELECT max(valeur), id_indicateur from odace.communair a inner join odace.commune c ON a.insee_com = c.code_insee 
                where annee = 2018 and code_epci ='".$_GET['ZoneBis']."'
                group by id_indicateur 
                order by id_indicateur";
}
else if ($_GET['Zone'] == 'Dep'){
    $name = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "SELECT annee, SUM(nox_tonne) as nox, sum(pm10_tonne)as pm10, sum(so2_tonne) as so2, sum(benzene_kg) as c6h6, sum(hap8_kg) as hap, sum(pm2_5_tonne) as pm2_5, sum(covnm_tonne) as covnm, sum(nh3_tonne) as nh3	
                from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
                WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_dep ='".$_GET['ZoneBis']."'
                group by annee
                order by annee ";
    $req2="SELECT max(valeur), id_indicateur from odace.communair a inner join odace.commune c ON a.insee_com = c.code_insee 
                where annee = 2018 and code_dep ='".$_GET['ZoneBis']."'
                group by id_indicateur 
                order by id_indicateur";
}
else if ($_GET['Zone'] == 'SCOT'){
    $name = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];

    $req = "SELECT annee, SUM(nox_tonne) as nox, sum(pm10_tonne)as pm10, sum(so2_tonne) as so2, sum(benzene_kg) as c6h6, sum(hap8_kg) as hap, sum(pm2_5_tonne) as pm2_5, sum(covnm_tonne) as covnm, sum(nh3_tonne) as nh3	
            from inventaire_emission.inventaire_pcaet a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
            WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and id_scot ='".$_GET['ZoneBis']."'
        group by annee
        order by annee ";
    $req2="SELECT max(valeur), id_indicateur from odace.communair a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
            where a.annee = 2018 and id_scot ='".$_GET['ZoneBis']."'
            group by id_indicateur 
            order by id_indicateur";

}
else {
    $name = "Centre-Val de Loire";
    $req = "SELECT annee, SUM(nox_tonne) as nox, sum(pm10_tonne)as pm10, sum(so2_tonne) as so2, sum(benzene_kg) as c6h6, sum(hap8_kg) as hap, sum(pm2_5_tonne) as pm2_5, sum(covnm_tonne) as covnm, sum(nh3_tonne) as nh3	
                from inventaire_emission.inventaire_pcaet
                WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' 
                group by annee
                order by annee ";
    $req2="SELECT max(valeur), id_indicateur from odace.communair a inner join odace.commune c ON a.insee_com = c.code_insee 
                where annee = 2018 
                group by id_indicateur 
                order by id_indicateur";
            
}
$evoNOX = [];
$evoC6H6 = [];
$evoSO2 = [];
$evoPM10 = [];
$evoHAP =[];
$evoNH3 = [];
$evoPM2_5 = [];
$evoCOVNM = [];
$annee =[];


$result = donnee($monPdo, $req);
foreach($result as $row){
    if ($row['annee'] == '2008'){
        $totdep = [
            "NOX" => $row['nox'],
            "C6H6" => $row['c6h6'],
            "SO2" => $row['so2'],
            "PM10" => $row['pm10'],
            "HAP" => $row['hap'],
            "NH3" => $row['nh3'],
            "PM2_5" => $row['pm2_5'],
            "COVNM" => $row['covnm']
        ];
        if ($totdep['NOX'] != 0){
            array_push($evoNOX, 100);
        }
        if ($totdep['C6H6'] != 0){
            array_push($evoC6H6, 100);
        }
        if ($totdep['SO2'] != 0){
            array_push($evoSO2, 100);
        }
        if ($totdep['PM10'] != 0){
            array_push($evoPM10, 100);
        }
        if ($totdep['HAP'] != 0){
            array_push($evoHAP, 100);
        }
        if ($totdep['NH3'] != 0){
            array_push($evoNH3, 100);
        }
        if ($totdep['PM2_5'] != 0){
            array_push($evoPM2_5, 100);
        }
        if ($totdep['COVNM'] != 0){
            array_push($evoCOVNM, 100);
        }
        array_push($annee, $row['annee']);
    }
    else{
        if ($totdep['NOX'] != 0){
            array_push($evoNOX,round(100*(($row['nox']- $totdep['NOX'])/ $totdep['NOX'])+100));
        }
        if ($totdep['C6H6'] != 0){
            array_push($evoC6H6, round(100*(($row['c6h6']- $totdep['C6H6'])/ $totdep['C6H6'])+100));
        }
        if ($totdep['SO2'] != 0){
            array_push($evoSO2, round(100*(($row['so2']- $totdep['SO2'])/ $totdep['SO2'])+100));
        }
        if ($totdep['PM10'] != 0){
            array_push($evoPM10, round(100*(($row['pm10']- $totdep['PM10'])/ $totdep['PM10'])+100));
        }
        if ($totdep['HAP'] != 0){
            array_push($evoHAP, round(100*(($row['hap']- $totdep['HAP'])/ $totdep['HAP'])+100));
        }
        if ($totdep['NH3'] != 0){
            array_push($evoNH3, round(100*(($row['nh3']- $totdep['NH3'])/ $totdep['NH3'])+100));
        }
        if ($totdep['PM2_5'] != 0){
            array_push($evoPM2_5, round(100*(($row['pm2_5']- $totdep['PM2_5'])/ $totdep['PM2_5'])+100));
        }
        if ($totdep['COVNM'] != 0){
            array_push($evoCOVNM, round(100*(($row['covnm']- $totdep['COVNM'])/ $totdep['COVNM'])+100));
        }
        array_push($annee, $row['annee']);
    }
}

echo '<script type="text/javascript">
       
        var dataEvoNOX ='.json_encode($evoNOX).';
        var dataEvoC6H6 ='.json_encode($evoC6H6).';
        var dataEvoPM10 ='.json_encode($evoPM10).';
        var dataEvoSO2 ='.json_encode($evoSO2).';
        var dataEvoHAP ='.json_encode($evoHAP).';
        var dataEvoNH3 ='.json_encode($evoNH3).';
        var dataEvoPM2_5 ='.json_encode($evoPM2_5).';
        var dataEvoCOVNM ='.json_encode($evoCOVNM).';
        var annee ='.json_encode($annee).';
        </script>';

            $Comm2018 = [];
            $result = donnee($monPdo, $req2);
            foreach($result as $row){
                    array_push($Comm2018,round($row[0],2,PHP_ROUND_HALF_UP));
            }

?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie3.css">

        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        
    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
           <div class='tBan'>
                <h3>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h3>
                <h4><?php echo $name?></h4>
                <h4>Concentrations et émissions de polluants à eﬀet sanitaire (PES)</h4>
            </div>
        </div>
        <div class="princBlock margin shadow-sm">
            <div class='sTitre'>
                <h5>Evolution des émissions des Polluants à Eﬀet Sanitaire depuis 2008</h5>
            </div>
            <div><p class="texte">Les évolutions sont présentées en base 100 par rapport à l’année de référence 2008. Ainsi les émissions de PES de 2008 ont été ﬁxées à 100 % pour constater les évolutions relatives sur les années suivantes.</p></div>
            <div id="chartEvoPES" class ="padding"></div>
        </div>
    <div class="princBlock margin shadow-sm" style="margin-bottom : 1%;">
        <div class='sTitre'>
            <h5>Bilan de la qualité de l’air et respect de la réglementation :</h5>
        </div>
        <p class='texte'> <?php if($Comm2018[3] <= 25){
                                    echo "En situation de fond (loin des sources émettrices), aucun dépassement des valeurs limites n’a été observé sur le territoire durant l’année 2018 pour les polluants atmosphériques NO2 (dioxyde d’azote), PM10 et O3 (ozone). Malgré le respect de ces valeurs, le territoire a fait l’objet d’épisodes de pollution en PM10 conduisant aux déclenchements de procédures préféctorales d’information et recommandation mais aussi d’alerte."; if($Comm2018[4]>6000){ echo ' Seul l’objectif de qualité pour l’ozone (AOT401) a été dépassé';}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            else {echo ' l’objectif de qualité pour l’ozone (AOT401) n\'a pas été dépassé';} 
                                }
                                else if($Comm2018[3] > 25){
                                    echo "En situation de fond (loin des sources émettrices), aucun dépassement des valeurs limites n’a été observé sur le territoire durant l’année 2018 pour les polluants atmosphériques NO2 (dioxyde d’azote) et PM10. Malgré le respect de ces valeurs, le territoire a fait l’objet d’épisodes de pollution en PM10 conduisant aux déclenchements de procédures préféctorales d’information et recommandation mais aussi d’alerte."; if($Comm2018[4]>6000){ echo ' L’objectif de qualité et la valeur cible pour l’ozone (AOT40 1) ont été dépassé.';}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            else {echo ' Seul  la valeur cible pour l’ozone (AOT401) a été dépassé';} 
                                }?>.</p>
        <div class="table">
            <table class="thead-dark shadow-sm">
                <thead>
                            <tr>
                                <th>Polluants</th>
                                <th>Indicateurs</th>
                                <th>Valeurs maximales dans le territoire (Valeurs réglementaires)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>NO2</td>
                                <td>Moyenne annuelle en situation de fond</td>
                                <td><?php echo $Comm2018[0] ?> µg/m3 (valeur limite : 40 µg/m3) </td>
                                
                               
                            </tr>
                            <tr>
                                <td>PM10</td>
                                <td>Moyenne annuelle</br>Nombre de jours dépassant 50 µg/m3</td>
                                <td><?php echo $Comm2018[1] ?> µg/m3 (valeur limite : 40 µg/m3)</br><?php echo $Comm2018[2] ?> jours (valeur limite : 35 jours par an)</td>
                            </tr>
                            <tr>
                                <td>O3</td>
                                <td>Nombre de jours >120 µg/m3 en moyenne sur 8h et 3 ans</br>AOT 40</td>
                                <td><?php echo $Comm2018[3] ?> jours (valeur limite : 25 jours par an)</br><?php echo $Comm2018[4] ?> µg/m3.h (objectif qualité : 6000 µg/m3.h)</td>
                            </tr>
                        </tbody>
                    </table>
                </div> 
    </div>
        
        <script src="../js/graphPage7.js"></script>
        <footer>
        <center>- 7 -</center>
    </footer>
    </body>
</html>