<?php
$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );


function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}

if ($_GET['Zone'] == 'EPCI'){
    $name = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="SELECT max(valeur) as val, id_indicateur, annee 
        from odace.communair a inner join odace.commune c ON a.insee_com = c.code_insee  
        where code_epci ='".$_GET['ZoneBis']."'
        group by id_indicateur, annee 
        order by id_indicateur, annee";
}
else if ($_GET['Zone'] == 'Dep'){
    $name = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="SELECT max(valeur) as val, id_indicateur, annee 
        from odace.communair a inner join odace.commune c ON a.insee_com = c.code_insee 
        where code_dep ='".$_GET['ZoneBis']."'
        group by id_indicateur, annee 
        order by id_indicateur, annee";
}
else if ($_GET['Zone'] == 'SCOT'){
    $name = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
     $req="SELECT max(valeur) as val, id_indicateur, annee 
     from odace.communair a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
     where id_scot ='".$_GET['ZoneBis']."'
     group by id_indicateur, annee 
     order by id_indicateur, annee";
}
else {
    $name = "Centre-Val de Loire";
    $req="SELECT max(valeur) as val, id_indicateur, annee 
        from odace.communair a inner join odace.commune c ON a.insee_com = c.code_insee 
        group by id_indicateur, annee 
        order by id_indicateur, annee";
}

$anneeBis = [];        


$result = donnee($monPdo, $req);
            foreach($result as $row){
                if (!in_array($row['annee'], $anneeBis)){
                    array_push($anneeBis, $row['annee']);
                    ${'Comm'.$row['annee']} = [];
                }
                    array_push(${'Comm'.$row['annee']},round($row['val'],2,PHP_ROUND_HALF_UP));
            }
            echo '<script type="text/javascript">
            var Comm2013 = '.json_encode($Comm2013).';
            var Comm2014 = '.json_encode($Comm2014).';
            var Comm2015 = '.json_encode($Comm2015).';
            var Comm2016 = '.json_encode($Comm2016).';
            var Comm2017 = '.json_encode($Comm2017).';
            var Comm2018 = '.json_encode($Comm2018).';
            var anneeBis = '.json_encode($anneeBis).'; 
            
            </script>';


?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie3.css">

        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        
    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
            <div class='tBan'>
                <h3>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h3>
                <h4><?php echo $name?></h4>
                <h4>Concentrations et émissions de polluants à eﬀet sanitaire (PES)</h4>
            </div>
        </div>
        <div class="methodo shadow-sm">
                <div class="sTMeth">
                    <h5>METHODOLOGIE</h5>
                </div>
                <div class="meth">
                    <p>Le bilan de la qualité de l’air sur le territoire est basé sur la modélisation nationale (Prev’Air) ou inter-régionale (Esmeralda). Les résultats bruts issus de cette modélisation sont aﬃnés statistiquement à partir des données d’observation issues des stations ﬁxes de Lig’Air. Le bilan ne concerne que les polluants faisant l’objet de modélisation. Les PM2.5, les HAP ainsi que d’autres polluants réglementés ne sont actuellement pas modélisés.</p>
                    <p>Pour plus de précisions, vous pouvez consulter les notes méthodologiques et/ou contacter LIG’AIR : ligair@ligair.fr.</p>
                </div>
            </div>
    <div class="princBlock margin shadow-sm" style="margin-bottom:1%;">
        <div class='sTitre'>
            <h5>Evolution des indicateurs réglementaires depuis 2013</h5>
        </div>
        <div class="flex-indic">
            <div id="chartIndic1"></div>
            <div id="chartIndic2"></div>
            <div id="chartIndic3"></div>
        </div>
        <div class="flex-indic">
            <div id="chartIndic4"></div>
            <div id="chartIndic5"></div>
        </div>
    </div>
        
        <script src="../js/graphPage8.js"></script>
        <footer>
        <center>- 8 -</center>
    </footer>
    </body>
</html>