@echo off
setlocal enabledelayedexpansion
for /d %%i in (*) do (
	set imgname=%%i\LEGENDE_population.png
	set imgname2=%%i\LEGENDE_population_2.png
	magick convert !imgname! -gravity East -chop 200x0 !imgname2!
	magick convert !imgname2! -gravity West -chop 70x0 !imgname2!
	magick convert !imgname2! -gravity North -chop 0x250 !imgname2!
	magick convert !imgname2! -gravity South -chop 0x200 !imgname2!
	set imgnamecarte=%%i\carte_situation.png
	set imgnamecarte2=%%i\carte_situation_2.png
	magick convert !imgnamecarte! -gravity East -chop 100x0 !imgnamecarte2!
	magick convert !imgnamecarte2! -gravity West -chop 150x0 !imgnamecarte2!
	magick convert !imgnamecarte2! -gravity North -chop 0x100 !imgnamecarte2!
	magick convert !imgnamecarte2! -gravity South -chop 0x100 !imgnamecarte2!
)