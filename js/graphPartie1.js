

Highcharts.chart('chartGES', {
  chart: {
      //backgroundColor: '#a39999',
      type: 'bar'
  },
  title: {
    style: {
      fontSize : "15px",
      fontWeight: 'bold',
    },
    text: 'GES ('+dataTotGes+' tonnes eq. CO2)'
  },

  exporting: { enabled: false },
  xAxis: {
    categories: dataSecteur,
    title: {
      text: null
    }
  },
  yAxis: {
    
    title: {
      text: '% GES',
      align: 'high'
    },
    labels: {
      overflow: 'justify'
    }
  },
  tooltip: {
    backgroundColor : 'rgba(35, 35, 35, 0.9)',
    style: {
      color: '#ffffff',},
          formatter: function(){
          
              return '<strong>'+this.x + '</strong><br/>'+dataGes[this.point.x]+'T eq CO2 | '+this.y+'%'
          }
       
  },
  plotOptions: {
    bar: {
      dataLabels: {
          formatter: function() {
              return  dataGes[this.point.x]+' T'
          }, 
        enabled: true
      }
    }
  },
  
  series: [
      {colorByPoint: true,
          data: dataStatGes,
          showInLegend: false
        }]
    });
 
      
  
    Highcharts.chart('chartCO2', {
      chart: {
          //backgroundColor: '#a39999',
          type: 'bar'
      },
      title: {
        style: {
          fontSize : "15px",
          fontWeight: 'bold',
        },
        text: 'CO2 <br/>('+dataTotCo2+' <br/>tonnes)'
      },
    
      exporting: { enabled: false },
      xAxis: {
        categories: dataSecteur,
        title: {
          text: null
        }
      },
      yAxis: {
        min : 0,
        max : 100,
        title: {
          text: '% CO2',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
        style: {
        color: '#ffffff',},
              formatter: function(){
              
                  return '<strong>'+this.x + '</strong><br/>'+dataCo2[this.point.x]+'T | '+this.y+'%'
              }
           
      },
      plotOptions: {
        bar: {
          dataLabels: {
              formatter: function() {
                  return  dataCo2[this.point.x]+' T'
              }, 
            enabled: true
          }
        }
      },
      
      series: [
          {colorByPoint: true,
              data: dataStatCo2,
              showInLegend: false
            }]
        });   

        Highcharts.chart('chartN2O', {
          chart: {
              //backgroundColor: '#a39999',
              type: 'bar'
          },
          title: {
            style: {
              fontSize : "15px",
              fontWeight: 'bold',
            },
            text: 'N2O <br/>('+dataTotN2o+' <br/>T eq. CO2)'
          },
        
          exporting: { enabled: false },
          xAxis: {
            categories: dataSecteur,
            title: {
              text: null
            },
            labels :{
            enabled: false
            }
          },
          yAxis: {
            min : 0,
            max : 100,
            title: {
              text: '% N2O',
              align: 'high'
            },
            labels: {
              overflow: 'justify',
            }
          },
          tooltip: {
                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                style: {
                color: '#ffffff',},
                  formatter: function(){
                  
                      return '<strong>'+this.x + '</strong><br/>'+dataN2o[this.point.x]+' T eq CO2| '+this.y+'%'
                  }
               
          },
          plotOptions: {
            bar: {
              dataLabels: {
                  formatter: function() {
                      return  dataN2o[this.point.x]+' T'
                  }, 
                enabled: true
              }
            }
          },
          
          series: [
              {colorByPoint: true,
                  data: dataStatN2o,
                  showInLegend: false
                }]
            });   
      
      
            Highcharts.chart('chartCH4', {
              chart: {
                  //backgroundColor: '#a39999',
                  type: 'bar'
              },
              title: {
                style: {
                  fontSize : "15px",
                  fontWeight: 'bold',
                },
                text: 'CH4 <br/>('+dataTotCh4+' <br/>T eq. CO2)'
              },
            
              exporting: { enabled: false },
              xAxis: {
                categories: dataSecteur,
                title: {
                  text: null
                },
                labels :{
                enabled: false
                }
              },
              yAxis: {
                min : 0,
                max : 100,
                title: {
                  text: '% CH4',
                  align: 'high'
                },
                labels: {
                  overflow: 'justify',
                }
              },
              tooltip: {
                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                style: {
                  color: '#ffffff',},
                      formatter: function(){
                      
                          return '<strong>'+this.x + '</strong><br/>'+dataCh4[this.point.x]+' T eq CO2| '+this.y+'%'
                      }
                   
              },
              plotOptions: {
                bar: {
                  dataLabels: {
                      formatter: function() {
                          return  dataCh4[this.point.x]+' T'
                      }, 
                    enabled: true
                  }
                }
              },
              
              series: [
                  {colorByPoint: true,
                      data: dataStatCh4,
                      showInLegend: false
                    }]
                });   


                Highcharts.chart('chartFLUORES', {
                  chart: {
                      //backgroundColor: '#a39999',
                      type: 'bar'
                  },
                  title: {
                    style: {
                      fontSize : "15px",
                      fontWeight: 'bold',
                    },
                    text: 'FLUORES <br/>('+dataTotFluore+' <br/>T eq. CO2)'
                  },
                
                  exporting: { enabled: false },
                  xAxis: {
                    categories: dataSecteur,
                    title: {
                      text: null
                    },
                    labels :{
                    enabled: false
                    }
                  },
                  yAxis: {
                    min : 0,
                    max : 100,
                    title: {
                      text: '% FLUORES',
                      align: 'high'
                    },
                    labels: {
                      overflow: 'justify',
                    }
                  },
                  tooltip: {
                    backgroundColor : 'rgba(35, 35, 35, 0.9)',
                    style: {
                    color: '#ffffff',},
                          formatter: function(){
                          
                              return '<strong>'+this.x + '</strong><br/>'+dataFluore[this.point.x]+' T eq CO2| '+this.y+'%'
                          }
                       
                  },
                  plotOptions: {
                    bar: {
                      dataLabels: {
                          formatter: function() {
                              return  dataFluore[this.point.x]+' T'
                          }, 
                        enabled: true
                      }
                    }
                  },
                  
                  series: [
                      {colorByPoint: true,
                          data: dataStatFluore,
                          showInLegend: false
                        }]
                    });   
  
              
                    Highcharts.chart('chartEvoGES', {

                      title: {
                        style: {
                          fontSize : "15px",
                          fontWeight: 'bold',
                        },
                        text: 'Evolution des émissions des GES'
                      },
                    
                    
                      exporting: { enabled: false },
                      plotOptions: {
                        series: {
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                              },
                              label: {
                                enabled: false,
                            },
                            },
                            
                      },
                      xAxis: {
                          categories: annee,
                          title: {
                              text: 'Année'
                          }
                      },
                      yAxis: {
                        title: {
                          text: 'Base 100 en 2008'
                        }
                      },
                      legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                      },
                  
                      
                      series: [{
                          name: "CO2",
                          data: dataEvoCo2
                      },
                      {
                          name: "N2O",
                          data: dataEvoN2o
                      },
                      {
                          name: "CH4",
                          data: dataEvoCh4
                      },
                      {
                          name: "FLUORES",
                          data: dataEvoFluore
                      }
                  ],
                  tooltip: {
                    backgroundColor : 'rgba(35, 35, 35, 0.9)',
                     style: {
                      color: '#ffffff',},
                      valueSuffix: ' %',
                          shared: true,
                          crosshairs: true
                      
                  },
                    
                      responsive: {
                        rules: [{
                          condition: {
                            maxWidth: 500
                          },
                          chartOptions: {
                            legend: {
                              layout: 'vertical',
                              align: 'right',
                              verticalAlign: 'middle'
                            },
                            
                          }
                        }]
                      }
                    
                    });    
              
          