        Highcharts.chart('chartPieCarbone', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            subtitle: {text: 'Source: <a href="https://www.ligair.fr/">Lig\'Air  / Inventaire des émissions 2016 v2.3 (juin 2019)</a>'},
            title : {
                text : null,
            },
            tooltip: {
                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                  style: {
                  color: '#ffffff',},
                pointFormat: '{point.y} ha | {point.percentage:.1f}%</b>'
            },
            
            exporting: { enabled: false },
            plotOptions: { 
                pie: {
                             
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Secteur',
                colorByPoint: true,
                data: [{
                    name: 'Hectares assimilés à l\'accroissement forestier',
                    y: totSurf[0]
                }, {
                    name: 'Hectares assimilés au changement d\'utilisation des terres',
                    y: totSurf[1]
                }, {
                    name: 'Hectares défrichés',
                    y: totSurf[2]
                },
                {
                    name: 'Hectares récoltés (industrie bois)',
                    y: totSurf[3]
                },
            ]
            }]
        });


        Highcharts.chart('chartColStackCarbone', {
            chart: {
                
                type: 'column'
            },
            title :{
                text : null,
            },
            subtitle: {text: 'Source: <a href="https://www.ligair.fr/">Lig\'Air</a>'},
            exporting: { enabled: false },
            xAxis: {
                categories: annee,
                title: {
                    text: "Année"
                }
            },
            yAxis: {
                
                title: {
                    text: 'T eq CO2'
        
                },
                stackLabels: {
                    enabled: false,
                   
                }
            },
            
            legend: {
                    
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                  style: {
                  color: '#ffffff',},
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y} T eq CO2 ({point.percentage:.1f} %)<br/>Total: {point.stackTotal} T eq CO2'
            },
            plotOptions: {
                column:{
                    stacking : 'normal',
                    
                },
               
            },
            series: [{
                color: '#f7a35c',
                name: 'Accroissement forestier',
                data: Carbone1
            }, {
                color: '#8085e9',
                name: 'Récolte du bois',
                data: Carbone16
            }, {
                color: '#f15c80',
                name: 'Defrichement',
                data: Carbone14
            },
            {
                color: '#e4d354',
                name: 'Changement d\'utilisation des terres',
                data: Carbone18
            },  
        ]
        });

        Highcharts.chart('chartEvoCarbone', {

            title: {
              text: null
            },
          
            subtitle: {text: 'Source: <a href="https://www.ligair.fr/">Lig\'Air</a>'},
            exporting: { enabled: false },
            plotOptions: {
                series: {
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                      },
                      label: {
                        enabled: false,
                    },
                    },
                    
              },
            xAxis: {
                categories: annee,
                title: {
                    text: 'Année'
                }
            },
            yAxis: {
              title: {
                text: 'T eq CO2'
              }
            },
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            },
        
            
            series: [
            {
                name: 'Neutralité carbone',
                data: TotDif
            },
            {
                name: 'Emissions de GES des grands secteur économiques',
                data: TotGES
            },
            {
                name: 'Séquestration nette',
                data: Carbone20
            },
            
            
        ],
        tooltip: {
          backgroundColor : 'rgba(35, 35, 35, 0.9)',
           style: {
            color: '#ffffff',},
            valueSuffix: ' T eq CO2',
                shared: true,
                crosshairs: true
            
        },
          
            responsive: {
              rules: [{
                condition: {
                  maxWidth: 500
                },
                chartOptions: {
                  legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                  }
                }
              }]
            }
          
          });   

var dataLab = ['Neutralité carbone','Emissions de GES des grands secteur économiques','Séquestration nette','Accroissement forestier','Récolte du bois', 'Defrichement','Changement d\'utilisation des terres'];
var dataVal = [TotDif[8],TotGES[8],Carbone20[8],Carbone1[8],Carbone16[8],Carbone14[8],Carbone18[8]];      
Highcharts.chart('chartBarCarbone', {
            chart: {
                
                type: 'bar'
            },
            title :{
                text: null
            },
            subtitle: {text: 'Source: <a href="https://www.ligair.fr/">Lig\'Air</a>'},
            exporting: { enabled: false },
            labels: {
                overflow: 'justify'
              },
            xAxis: {
                categories: dataLab,
                title: {
                  text: null
                },
            },
            yAxis: {
                
                title: {
                    text: ''
        
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            
        
            tooltip: {
                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                  style: {
                  color: '#ffffff',},
                  formatter: function(){
                    return '<strong>'+this.x + '</strong><br/>'+this.y+' T eq CO2'
                }
            },
            plotOptions: {
                bar: {
                dataLabels: {
                    formatter: function() {
                        return  this.y+' T'
                    }, 
                enabled: true
                }
            }
            },
            series: [
                {colorByPoint: true,
                    data: dataVal,
                    showInLegend: false
                  },
                 
        ],
        });
