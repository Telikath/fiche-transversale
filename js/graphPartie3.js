Highcharts.chart('chartNOx', {
  chart: {
      //backgroundColor: '#a39999',
      type: 'bar'
  },
  title: {
    style: {
      fontSize : "15px",
      fontWeight: 'bold',
    },
    text: 'NOx <br/>('+dataTotNOX+' tonnes)'
  },

  exporting: { enabled: false },
  xAxis: {
    categories: dataSecteur,
    title: {
      text: null
    },
  },
  yAxis: {
    min : 0,
    max : 100,
    title: {
      text: '% NOx',
      align: 'high'
    },
    labels: {
      overflow: 'justify',
    }
  },
  tooltip: {
    backgroundColor : 'rgba(35, 35, 35, 0.9)',
    style: {
      color: '#ffffff',},
          formatter: function(){
          
              return '<strong>'+this.x + '</strong><br/>'+dataNOX[this.point.x]+' T | '+this.y+'%'
          }
       
  },
  plotOptions: {
    bar: {
      dataLabels: {
          formatter: function() {
              return  dataNOX[this.point.x]+' T'
          }, 
        enabled: true
      }
    }
  },
  
  series: [
      {colorByPoint: true,
          data: dataStatNOX,
          showInLegend: false
        }]
    });   


    Highcharts.chart('chartPM10', {
      chart: {
          //backgroundColor: '#a39999',
          type: 'bar'
      },
      title: {
        style: {
          fontSize : "15px",
          fontWeight: 'bold',
        },
        text: 'PM10 <br/>('+dataTotPM10+' tonnes)'
      },
    
      exporting: { enabled: false },
      xAxis: {
        categories: dataSecteur,
        title: {
          text: null
        },
        labels :{
        enabled: false
        }
      },
      yAxis: {
        min : 0,
        max : 100,
        title: {
          text: '% PM10',
          align: 'high'
        },
        labels: {
          overflow: 'justify',
        }
      },
      tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
        style: {
          color: '#ffffff',},
              formatter: function(){
              
                  return '<strong>'+this.x + '</strong><br/>'+dataPM10[this.point.x]+' T | '+this.y+'%'
              }
           
      },
      plotOptions: {
        bar: {
          dataLabels: {
              formatter: function() {
                  return  dataPM10[this.point.x]+' T'
              }, 
            enabled: true
          }
        }
      },
      
      series: [
          {colorByPoint: true,
              data: dataStatPM10,
              showInLegend: false
            }]
        });  



        Highcharts.chart('chartSO2', {
          chart: {
              //backgroundColor: '#a39999',
              type: 'bar'
          },
          title: {
            style: {
              fontSize : "15px",
              fontWeight: 'bold',
            },
            text: 'SO2 <br/>('+dataTotSO2+' tonnes)'
          },
        
          exporting: { enabled: false },
          xAxis: {
            categories: dataSecteur,
            title: {
              text: null
            },
            labels :{
            enabled: false
            }
          },
          yAxis: {
            min : 0,
            max : 100,
            title: {
              text: '% SO2',
              align: 'high'
            },
            labels: {
              overflow: 'justify',
            }
          },
          tooltip: {
            backgroundColor : 'rgba(35, 35, 35, 0.9)',
            style: {
              color: '#ffffff',},
                  formatter: function(){
                  
                      return '<strong>'+this.x + '</strong><br/>'+dataSO2[this.point.x]+' T | '+this.y+'%'
                  }
               
          },
          plotOptions: {
            bar: {
              dataLabels: {
                  formatter: function() {
                      return  dataSO2[this.point.x]+' T'
                  }, 
                enabled: true
              }
            }
          },
          
          series: [
              {colorByPoint: true,
                  data: dataStatSO2,
                  showInLegend: false
                }]
            });            


            Highcharts.chart('chartC6H6', {
              chart: {
                  //backgroundColor: '#a39999',
                  type: 'bar'
              },
              title: {
                style: {
                  fontSize : "15px",
                  fontWeight: 'bold',
                },
                text: 'C6H6 <br/>('+dataTotC6H6+' kg)'
              },
            
              exporting: { enabled: false },
              xAxis: {
                categories: dataSecteur,
                title: {
                  text: null
                },
                labels :{
                enabled: false
                }
              },
              yAxis: {
                min : 0,
                max : 100,
                title: {
                  text: '% C6H6',
                  align: 'high'
                },
                labels: {
                  overflow: 'justify',
                }
              },
              tooltip: {
                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                style: {
                  color: '#ffffff',},
                      formatter: function(){
                      
                          return '<strong>'+this.x + '</strong><br/>'+dataC6H6[this.point.x]+' kg | '+this.y+'%'
                      }
                   
              },
              plotOptions: {
                bar: {
                  dataLabels: {
                      formatter: function() {
                          return  dataC6H6[this.point.x]+' kg'
                      }, 
                    enabled: true
                  }
                }
              },
              
              series: [
                  {colorByPoint: true,
                      data: dataStatC6H6,
                      showInLegend: false
                    }]
                });  

                Highcharts.chart('chartHAP', {
                  chart: {
                      //backgroundColor: '#a39999',
                      type: 'bar'
                  },
                  title: {
                    style: {
                      fontSize : "15px",
                      fontWeight: 'bold',
                    },
                    text: 'HAP <br/>('+dataTotHAP+' kg)'
                  },
                
                  exporting: { enabled: false },
                  xAxis: {
                    categories: dataSecteur,
                    title: {
                      text: null
                    },
                    
                  },
                  yAxis: {
                    min : 0,
                    max : 100,
                    title: {
                      text: '% HAP',
                      align: 'high'
                    },
                    labels: {
                      overflow: 'justify',
                    }
                  },
                  tooltip: {
                    backgroundColor : 'rgba(35, 35, 35, 0.9)',
                    style: {
                      color: '#ffffff',},
                          formatter: function(){
                          
                              return '<strong>'+this.x + '</strong><br/>'+dataHAP[this.point.x]+' kg | '+this.y+'%'
                          }
                       
                  },
                  plotOptions: {
                    bar: {
                      dataLabels: {
                          formatter: function() {
                              return  dataHAP[this.point.x]+' kg'
                          }, 
                        enabled: true
                      }
                    }
                  },
                  
                  series: [
                      {colorByPoint: true,
                          data: dataStatHAP,
                          showInLegend: false
                        }]
                    });  

                    Highcharts.chart('chartPM2_5', {
                      chart: {
                          //backgroundColor: '#a39999',
                          type: 'bar'
                      },
                      title: {
                        style: {
                          fontSize : "15px",
                          fontWeight: 'bold',
                        },
                        text: 'PM2.5 <br/>('+dataTotPM2_5+' tonnes)'
                      },
                    
                      exporting: { enabled: false },
                      xAxis: {
                        categories: dataSecteur,
                        title: {
                          text: null
                        },
                        labels :{
                        enabled: false
                        }
                      },
                      yAxis: {
                        min : 0,
                        max : 100,
                        title: {
                          text: '% PM2.5',
                          align: 'high'
                        },
                        labels: {
                          overflow: 'justify',
                        }
                      },
                      tooltip: {
                        backgroundColor : 'rgba(35, 35, 35, 0.9)',
                        style: {
                          color: '#ffffff',},
                              formatter: function(){
                              
                                  return '<strong>'+this.x + '</strong><br/>'+dataPM2_5[this.point.x]+' T | '+this.y+'%'
                              }
                           
                      },
                      plotOptions: {
                        bar: {
                          dataLabels: {
                              formatter: function() {
                                  return  dataPM2_5[this.point.x]+' T'
                              }, 
                            enabled: true
                          }
                        }
                      },
                      
                      series: [
                          {colorByPoint: true,
                              data: dataStatPM2_5,
                              showInLegend: false
                            }]
                        });  

                        Highcharts.chart('chartCOVNM', {
                          chart: {
                              //backgroundColor: '#a39999',
                              type: 'bar'
                          },
                          title: {
                            style: {
                              fontSize : "15px",
                              fontWeight: 'bold',
                            },
                            text: 'COVNM <br/>('+dataTotCOVNM+' tonnes)'
                          },
                        
                          exporting: { enabled: false },
                          xAxis: {
                            categories: dataSecteur,
                            title: {
                              text: null
                            },
                            labels :{
                            enabled: false
                            }
                          },
                          yAxis: {
                            min : 0,
                            max : 100,
                            title: {
                              text: '% COVNM',
                              align: 'high'
                            },
                            labels: {
                              overflow: 'justify',
                            }
                          },
                          tooltip: {
                            backgroundColor : 'rgba(35, 35, 35, 0.9)',
                            style: {
                              color: '#ffffff',},
                                  formatter: function(){
                                  
                                      return '<strong>'+this.x + '</strong><br/>'+dataCOVNM[this.point.x]+' T | '+this.y+'%'
                                  }
                               
                          },
                          plotOptions: {
                            bar: {
                              dataLabels: {
                                  formatter: function() {
                                      return  dataCOVNM[this.point.x]+' T'
                                  }, 
                                enabled: true
                              }
                            }
                          },
                          
                          series: [
                              {colorByPoint: true,
                                  data: dataStatCOVNM,
                                  showInLegend: false
                                }]
                            });  
                            Highcharts.chart('chartNH3', {
                              chart: {
                                  //backgroundColor: '#a39999',
                                  type: 'bar'
                              },
                              title: {
                                style: {
                                  fontSize : "15px",
                                  fontWeight: 'bold',
                                },
                                text: 'NH3 <br/>('+dataTotNH3+' tonnes)'
                              },
                            
                              exporting: { enabled: false },
                              xAxis: {
                                categories: dataSecteur,
                                title: {
                                  text: null
                                },
                                labels :{
                                enabled: false
                                }
                              },
                              yAxis: {
                                min : 0,
                                max : 100,
                                title: {
                                  text: '% NH3',
                                  align: 'high'
                                },
                                labels: {
                                  overflow: 'justify',
                                }
                              },
                              tooltip: {
                                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                                style: {
                                  color: '#ffffff',},
                                      formatter: function(){
                                      
                                          return '<strong>'+this.x + '</strong><br/>'+dataNH3[this.point.x]+' T | '+this.y+'%'
                                      }
                                   
                              },
                              plotOptions: {
                                bar: {
                                  dataLabels: {
                                      formatter: function() {
                                          return  dataNH3[this.point.x]+' T'
                                      }, 
                                    enabled: true
                                  }
                                }
                              },
                              
                              series: [
                                  {colorByPoint: true,
                                      data: dataStatNH3,
                                      showInLegend: false
                                    }]
                                });


                                Highcharts.chart('chartEvoPES', {

                                  title: {
                                    style: {
                                      fontSize : "15px",
                                      fontWeight: 'bold',
                                    },
                                    text: 'Evolution des émissions des PES'
                                  },
                                
                                
                                  exporting: { enabled: false },
                                  plotOptions: {
                                    series: {
                                        marker: {
                                            enabled: false,
                                            symbol: 'circle',
                                            states: {
                                                hover: {
                                                    enabled: true
                                                }
                                            }
                                          },
                                          label: {
                                            enabled: false,
                                        },
                                        },
                                        
                                  },
                                  xAxis: {
                                      categories: annee,
                                      title: {
                                          text: 'Année'
                                      }
                                  },
                                  yAxis: {
                                    title: {
                                      text: 'Base 100 en 2008'
                                    }
                                  },
                                  legend: {
                                    layout: 'vertical',
                                    align: 'right',
                                    verticalAlign: 'middle'
                                  },
                              
                                  
                                  series: [{
                                      name: "NOx",
                                      data: dataEvoNOX
                                  },
                                  {
                                      name: "PM10",
                                      data: dataEvoPM10
                                  },
                                  {
                                      name: "SO2",
                                      data: dataEvoSO2
                                  },
                                  {
                                      name: "C6H6",
                                      data: dataEvoC6H6
                                  },
                                  {
                                      name: "HAP",
                                      data: dataEvoHAP
                                  },
                                  {
                                      name: "PM2.5",
                                      data: dataEvoPM2_5
                                  },
                                  {
                                      name: "COVNM",
                                      data: dataEvoCOVNM
                                  },
                                  {
                                      name: "NH3",
                                      data: dataEvoNH3
                                  }
                              ],
                              tooltip: {
                                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                                 style: {
                                  color: '#ffffff',},
                                  valueSuffix: ' %',
                                      shared: true,
                                      crosshairs: true
                                  
                              },
                                
                                  responsive: {
                                    rules: [{
                                      condition: {
                                        maxWidth: 500
                                      },
                                      chartOptions: {
                                        legend: {
                                          layout: 'horizontal',
                                          align: 'center',
                                          verticalAlign: 'bottom'
                                        }
                                      }
                                    }]
                                  }
                                
                                });   

                                Highcharts.chart('chartIndic1', {
                                  chart: {
                                      //backgroundColor: '#a39999',
                                      type: 'column'
                                  },
                                  title: {
                                    style: {
                                      fontSize : "15px",
                                      fontWeight: 'bold',
                                    },
                                    text: 'Valeurs maximales de la moyenne annuelle en NO2 sur le territoire'
                                  },
                                
                                  exporting: { enabled: false },
                                  xAxis: {
                                    categories: anneeBis,
                                    title: {
                                      text: null
                                    }
                                  },
                                  yAxis: {
                                    max:45,
                                    title:{
                                      text : 'Concentrations (en µg/m3)'
                                    },
                                    labels: {
                                      overflow: 'justify'
                                    },
                                    plotLines: [{
                                      label :{text: 'VL=40 µg/m3',},
                                      
                                      color: 'red', // Color value
                                      value: 40, // Value of where the line will appear
                                      width: 2, // Width of the line  
                                                      zIndex: 5,     
                                    }]
                                  },
                                  tooltip: {
                                    backgroundColor : 'rgba(35, 35, 35, 0.9)',
                                    style: {
                                      color: '#ffffff',},
                                          formatter: function(){
                                          
                                              return '<strong>'+this.x + '</strong><br/>'+this.y+' µg/m3'
                                          }
                                       
                                  },
                                  plotOptions: {
                                    column: {
                                      dataLabels: {
                                          formatter: function() {
                                              return  this.y
                                          }, 
                                        enabled: true
                                      }
                                    }
                                  },
                                  
                                  series: [
                                      {colorByPoint: true,
                                          data: [Comm2013[0],Comm2014[0],Comm2015[0],Comm2016[0],Comm2017[0],Comm2018[0]],
                                          showInLegend: false
                                        }]
                                    });
                              
                                    Highcharts.chart('chartIndic2', {
                                      chart: {
                                          //backgroundColor: '#a39999',
                                          type: 'column'
                                      },
                                      title: {
                                        style: {
                                          fontSize : "15px",
                                          fontWeight: 'bold',
                                        },
                                        text: 'Valeurs maximales de la moyenne annuelle en PM10 sur le territoire'
                                      },
                                    
                                      exporting: { enabled: false },
                                      xAxis: {
                                        categories: anneeBis,
                                        title: {
                                          text: null
                                        }
                                      },
                                      yAxis: {
                                        max:45,
                                        title:{
                                          text : 'Concentrations (en µg/m3)'
                                        },
                                        labels: {
                                          overflow: 'justify'
                                        },
                                        plotLines: [{
                                          label :{text: 'VL=40 µg/m3',},
                                          
                                          color: 'red', // Color value
                                          value: 40, // Value of where the line will appear
                                          width: 2, // Width of the line  
                                                      zIndex: 5,   
                                        }]
                                      },
                                      tooltip: {
                                        backgroundColor : 'rgba(35, 35, 35, 0.9)',
                                        style: {
                                          color: '#ffffff',},
                                              formatter: function(){
                                              
                                                  return '<strong>'+this.x + '</strong><br/>'+this.y+' µg/m3'
                                              }
                                           
                                      },
                                      plotOptions: {
                                        column: {
                                          dataLabels: {
                                              formatter: function() {
                                                  return  this.y
                                              }, 
                                            enabled: true
                                          }
                                        }
                                      },
                                      
                                      series: [
                                          {colorByPoint: true,
                                              data: [Comm2013[1],Comm2014[1],Comm2015[1],Comm2016[1],Comm2017[1],Comm2018[1]],
                                              showInLegend: false
                                            }]
                                        });
                              
                                        Highcharts.chart('chartIndic3', {
                                          chart: {
                                              //backgroundColor: '#a39999',
                                              type: 'column'
                                          },
                                          title: {
                                            style: {
                                              fontSize : "15px",
                                              fontWeight: 'bold',
                                            },
                                            text: 'Nombre maximal de jours dépassant 50 µg/m3 en PM10 sur le territoire'
                                          },
                                        
                                          exporting: { enabled: false },
                                          xAxis: {
                                            categories: anneeBis,
                                            title: {
                                              text: null
                                            }
                                          },
                                          yAxis: {
                                            max:40,
                                            title:{
                                              text : 'Nombre de jours de dépassement'
                                            },
                                            labels: {
                                              overflow: 'justify'
                                            },
                                            plotLines: [{
                                              label :{text: 'VL=35 jours/an',},
                                              
                                              color: 'red', // Color value
                                              value: 35, // Value of where the line will appear
                                              width: 2, // Width of the line  
                                                      zIndex: 5,   
                                            }]
                                          },
                                          tooltip: {
                                            backgroundColor : 'rgba(35, 35, 35, 0.9)',
                                            style: {
                                              color: '#ffffff',},
                                                  formatter: function(){
                                                  
                                                      return '<strong>'+this.x + '</strong><br/>'+this.y+' jours/an'
                                                  }
                                               
                                          },
                                          plotOptions: {
                                            column: {
                                              dataLabels: {
                                                  formatter: function() {
                                                      return  this.y
                                                  }, 
                                                enabled: true
                                              }
                                            }
                                          },
                                          
                                          series: [
                                              {colorByPoint: true,
                                                  data: [Comm2013[2],Comm2014[2],Comm2015[2],Comm2016[2],Comm2017[2],Comm2018[2]],
                                                  showInLegend: false
                                                }]
                                            });
                              
                                            Highcharts.chart('chartIndic4', {
                                              chart: {
                                                  //backgroundColor: '#a39999',
                                                  type: 'column'
                                              },
                                              title: {
                                                style: {
                                                  fontSize : "15px",
                                                  fontWeight: 'bold',
                                                },
                                                text: 'Nombre maximal de jours dépassant 120 µg/m3 en O3 (en moyenne sur 8h et sur 3 ans) sur le territoire'
                                              },
                                            
                                              exporting: { enabled: false },
                                              xAxis: {
                                                categories: anneeBis,
                                                title: {
                                                  text: null
                                                }
                                              },
                                              yAxis: {
                                                max:30,
                                                title:{
                                                  text : 'Nombre de jours de dépassement'
                                                },
                                                labels: {
                                                  overflow: 'justify'
                                                },
                                                plotLines: [{
                                                  label :{text: 'VC=25 jours/an',},
                                                  
                                                  color: 'red', // Color value
                                                  value: 25, // Value of where the line will appear
                                                  width: 2, // Width of the line  
                                                      zIndex: 5,    
                                                }]
                                              },
                                              tooltip: {
                                                backgroundColor : 'rgba(35, 35, 35, 0.9)',
                                                style: {
                                                  color: '#ffffff',},
                                                      formatter: function(){
                                                      
                                                          return '<strong>'+this.x + '</strong><br/>'+this.y+' jours/an'
                                                      }
                                                   
                                              },
                                              plotOptions: {
                                                column: {
                                                  dataLabels: {
                                                      formatter: function() {
                                                          return  this.y
                                                      }, 
                                                    enabled: true
                                                  }
                                                }
                                              },
                                              
                                              series: [
                                                  {colorByPoint: true,
                                                      data: [Comm2013[3],Comm2014[3],Comm2015[3],Comm2016[3],Comm2017[3],Comm2018[3]],
                                                      showInLegend: false
                                                    }]
                                                });
                              
                                                Highcharts.chart('chartIndic5', {
                                                  chart: {
                                                      //backgroundColor: '#a39999',
                                                      type: 'column'
                                                  },
                                                  title: {
                                                    style: {
                                                      fontSize : "15px",
                                                      fontWeight: 'bold',
                                                    },
                                                    text: 'Valeur maximale de l\'AOT40 en O3 sur le territoire'
                                                  },
                                                
                                                  exporting: { enabled: false },
                                                  xAxis: {
                                                    categories: anneeBis,
                                                    title: {
                                                      text: null
                                                    }
                                                  },
                                                  yAxis: {
                                                    
                                                    title:{
                                                      text : 'en µg/m3.h '
                                                    },
                                                    labels: {
                                                      overflow: 'justify'
                                                    },
                                                    plotLines: [{
                                                      label :{text: 'Objectif=6000 µg/m3.h',},
                                                      
                                                      color: 'red', // Color value
                                                      value: 6000, // Value of where the line will appear
                                                      width: 2, // Width of the line  
                                                      zIndex: 5,  
                                                    }]
                                                  },
                                                  tooltip: {
                                                    backgroundColor : 'rgba(35, 35, 35, 0.9)',
                                                    style: {
                                                      color: '#ffffff',},
                                                          formatter: function(){
                                                          
                                                              return '<strong>'+this.x + '</strong><br/>'+this.y+' µg/m3.h'
                                                          }
                                                       
                                                  },
                                                  plotOptions: {
                                                    column: {
                                                      dataLabels: {
                                                          formatter: function() {
                                                              return  this.y
                                                          }, 
                                                        enabled: true
                                                      }
                                                    }
                                                  },
                                                  
                                                  series: [
                                                      {colorByPoint: true,
                                                          data: [Comm2013[4],Comm2014[4],Comm2015[4],Comm2016[4],Comm2017[4],Comm2018[4]],
                                                          showInLegend: false
                                                        }]
                                                    });
                              
                                 
                                                             