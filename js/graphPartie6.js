Highcharts.chart('chartColStackProd', {
    chart: {
        height : 500,
        type: 'column'
    },
    title :{
        text: null
    },
    
    exporting: { enabled: false },
    xAxis: {
        categories: annee,
    },
    yAxis: {
        min: 0,
        title: {
            text: 'GWh'

        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }
    },
    legend: {
            title: {
                text: 'Filières d\'ENR',
                style: {
                    fontStyle: 'italic'
                }
            },
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: false,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
          color: '#ffffff',},
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y} GWh ({point.percentage:.1f} %)<br/>Total: {point.stackTotal} GWh'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    formatter: function () {
                        return Math.round(100 * this.y / this.total) + '%';
                 }, 
            }
        }
    },
    series: [{
        name: 'Biomasse thermique',
        data: totProd7
    }, {
        name: 'Biomasse electrique',
        data: totProd2
    }, {
        name: 'Biomethane',
        data: totProd11
    },
    {
        name: 'Eolien',
        data: totProd3
    }, {
        name: 'Solaire photovoltaique',
        data: totProd5
    }, {
        name: 'Solaire thermique',
        data: totProd6
    },
    {
        name: 'Geothermie',
        data: totProd1
    },
    {
        name: 'Hydraulique',
        data: totProd4
    }
]
});

Highcharts.chart('chartPieProd', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title :{
        text: null
    },
    subtitle: {text: 'Source: <a href="https://www.ligair.fr/">Lig\'Air</a>'},
    tooltip: {
        backgroundColor : 'rgba(35, 35, 35, 0.9)',
          style: {
          color: '#ffffff',},
        pointFormat: '{point.y} GWh | {point.percentage:.1f}%</b>'
    },
    
    exporting: { enabled: false },
    plotOptions: {
        pie: {
                     
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
            
                enabled: true,
                allowOverlap: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Secteur',
        colorByPoint: true,
        data: [{
            name: 'Biomasse thermique',
            y: totProd7[2]
        }, {
            name: 'Biomasse electrique',
            y: totProd2[2]
        }, {
            name: 'Biomethane',
            y: totProd11[2]
        },
        {
            name: 'Eolien',
            y: totProd3[2]
        }, {
            name: 'Solaire photovoltaique',
            y: totProd5[2]
        }, {
            name: 'Solaire thermique',
            y: totProd6[2]
        },
        {
            name: 'Geothermie',
            y: totProd1[2]
        },
        {
            name: 'Hydraulique',
            y: totProd4[2]
        }
    ]
    }]
});


Highcharts.chart('chartLineBilan', {

    title: {
      text: null
    },
  
    subtitle: {text: 'Source: <a href="https://www.ligair.fr/">Lig\'Air</a>'},
    exporting: { enabled: false },
    plotOptions: {
        series: {
            marker: {
                enabled: false,
                symbol: 'circle',
                states: {
                    hover: {
                        enabled: true
                    }
                }
              },
              label: {
                enabled: false,
            },
            },
            
      },
    xAxis: {
        categories: annee,
        title: {
            text: 'Année'
        }
    },
    yAxis: {
      title: {
        text: 'GWh'
      }
    },
    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'bottom'
    },

    
    series: [
    {
        name: 'Production ENR totale',
        data: productFinal
    },
    {
        name: 'Consommation énergetique finale totale',
        data: consoFinal
    },
    
],
tooltip: {
  backgroundColor : 'rgba(35, 35, 35, 0.9)',
   style: {
    color: '#ffffff',},
    valueSuffix: ' GWh',
        shared: true,
        crosshairs: true
    
},
  
    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  
  });   