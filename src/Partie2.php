<?php
$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );

function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}

if ($_GET['Zone'] == 'EPCI'){
    $name = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "SELECT sum(a.valeur) as val, a.id_type 
        from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee 
        where a.id_type in (2,15,19,17) and a.annee = 2016 and c.code_epci ='".$_GET['ZoneBis']."'
        group by a.id_type
        order by a.id_type";

    $req2="SELECT sum(a.valeur) as val , a.id_type as t, a.annee as annee from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee 
    where a.id_type in (1,18,14,16,20) and code_epci ='".$_GET['ZoneBis']."'
    group by a.id_type, a.annee
    order by a.id_type, a.annee"; 
        
    $req3="SELECT sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as val, annee
    from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
    WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_epci ='".$_GET['ZoneBis']."'
    group by annee
    order by annee";
}
else if ($_GET['Zone'] == 'Dep'){
    $name = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "SELECT sum(a.valeur) as val, a.id_type 
        from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee 
        where a.id_type in (2,15,19,17) and a.annee = 2016 and code_dep ='".$_GET['ZoneBis']."'
        group by a.id_type
        order by a.id_type";

$req2="SELECT sum(a.valeur) as val , a.id_type as t, a.annee as annee from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee 
where a.id_type in (1,18,14,16,20) and code_dep ='".$_GET['ZoneBis']."'
group by a.id_type, a.annee
order by a.id_type, a.annee"; 
    
$req3="SELECT sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as val, annee
from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_dep ='".$_GET['ZoneBis']."'
group by annee
order by annee";
}
else if ($_GET['Zone'] == 'SCOT'){
    $name = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "SELECT sum(a.valeur) as val, a.id_type 
        from odace.sequestration a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
        where a.id_type in (2,15,19,17) and a.annee = 2016 and id_scot ='".$_GET['ZoneBis']."'
        group by a.id_type
        order by a.id_type";

    $req2="SELECT sum(a.valeur) as val , a.id_type as t, a.annee as annee from odace.sequestration a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
    where a.id_type in (1,18,14,16,20) and id_scot ='".$_GET['ZoneBis']."'
    group by a.id_type, a.annee
    order by a.id_type, a.annee"; 
        
    $req3="SELECT sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as val, annee
    from inventaire_emission.inventaire_pcaet a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
    WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and id_scot ='".$_GET['ZoneBis']."'
    group by annee
    order by annee";
}
else {
    $name = "Centre-Val de Loire";
    $req = "SELECT sum(a.valeur) as val, a.id_type 
        from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee 
        where a.id_type in (2,15,19,17) and a.annee = 2016
        group by a.id_type
        order by a.id_type";

    $req2="SELECT sum(a.valeur) as val , a.id_type as t, a.annee as annee from odace.sequestration a inner join odace.commune c ON a.insee_com = c.code_insee 
    where a.id_type in (1,18,14,16,20) 
    group by a.id_type, a.annee
    order by a.id_type, a.annee"; 
    
    $req3="SELECT sum(co2_tonne +ch4_teqco2 +n2o_teqco2 +pfc_teqco2+hfc_teqco2+sf6_teqco2+nf3_teqco2) as val, annee
    from inventaire_emission.inventaire_pcaet
    WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus'
    group by annee
    order by annee";     
    
}


$totCarboneSurf =[];
$result = donnee($monPdo, $req);
        foreach($result as $row){
            array_push($totCarboneSurf,round($row['val'],0,PHP_ROUND_HALF_UP));
        }       

echo '<script type="text/javascript">
var totSurf = '.json_encode($totCarboneSurf).';
</script>';  
    
           
           $annee = [];
                
$result = donnee($monPdo, $req2);
        $typeCourant = "";
        foreach($result as $row){
            if ($typeCourant != $row['t']){
                $typeCourant = $row['t'];
                ${'Carbone'.$typeCourant} = [];
                array_push(${'Carbone'.$typeCourant},round($row['val'],0,PHP_ROUND_HALF_UP));
                array_push($annee, $row['annee']);
            }
            else {
                array_push(${'Carbone'.$typeCourant},round($row['val'],0,PHP_ROUND_HALF_UP));
                array_push($annee, $row['annee']);
            }
                
        }

        echo '<script type="text/javascript">
        var Carbone1 = '.json_encode($Carbone1).';
        var Carbone18 = '.json_encode($Carbone18).';
        var Carbone16 = '.json_encode($Carbone16).';
        var Carbone14 = '.json_encode($Carbone14).';
        var Carbone20 = '.json_encode($Carbone20).';
        var annee = '.json_encode($annee).';
           </script>'; 
      
        $TotGES = [];
        $i=0;
        $TotDif = []; 
        $result = donnee($monPdo, $req3);
        foreach($result as $row){
            array_push($TotGES,round($row['val'],0,PHP_ROUND_HALF_UP));            
            array_push($TotDif,(round($row['val'],0,PHP_ROUND_HALF_UP)+$Carbone20[$i]));
            $i+=1;
            }
        echo '<script type="text/javascript">
                var TotGES= '.json_encode($TotGES).';
                var TotDif= '.json_encode($TotDif).';
                   </script>';  
           
?>           



<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie2.css">


        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        
    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
            <div><a class="fa fa-chevron-left icon-left fa-2x valid" <?php echo "href='Partie1.php?Zone=".$_GET['Zone']."&ZoneBis=".$_GET['ZoneBis']."'"?>></a></div>
            <div class='tBan'>
                <h2>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h2>
                <h3><?php echo $name?></h3>
                <h3>Estimation de la séquestration carbone du territoire</h3>
            </div>
            <div class="bt "><button class="btn btn-success"> Télécharger la fiche</button></div>
            <div><a class="fa fa-chevron-right icon-right fa-2x valid" <?php echo "href='Partie3.php?Zone=".$_GET['Zone']."&ZoneBis=".$_GET['ZoneBis']."'"?>></a></div>
            
        </div>

        <div class="methodo shadow-sm">
                <div class="sTMeth">
                    <h5>METHODOLOGIE</h5>
                </div>
                <div class="meth">
                    <p>La méthodologie appliquée pour l’estimation de la « séquestration carbone » s’inspire de méthodes de références notamment celles énoncées par le GIEC et le CITEPA. Les émissions et absorptions de CO2 de ce sous-secteur correspondent à des ﬂux et non des stocks. Pour une estimation des stocks de carbone sur votre territoire, l’ADEME a développé l’outil ALDO [cf. complément d’information et liens utiles]. ALDO estime également des ﬂux mais selon des périmètres diﬀérents de ceux présentés ici. Les deux outils sont donc à utiliser de façon complémentaire plutôt qu’à des ﬁns de compraison. Les puits de carbone sur un territoire sont, ici, estimés à travers quatre ﬂux : l’accroissement forestier (absorptions), la récolte de bois (émissions), le défrichement (émissions) et les changements d’utilisation des sols (émissions et absorptions). La séquestration nette de carbone est identiﬁé comme un secteur dans l’inventaire des émissions de Lig’Air intitulé « secteur UTCATF (Utilisation des Terres, Changement d’Aﬀectation des Terres et Foresterie) ».</p>
                    <p>La séquestration nette correspond à la somme de ces absorptions et émissions. La somme de la séquestration nette et des émissions de GES des grands secteurs économiques présentés précédemment permet d’évaluer l’atteinte de la neutralité carbone du territoire. </p>
                    <p>La neutralité carbone étant un équilibre à atteindre entre les absorptions de carbone dans les écosystèmes gérés par l’homme (secteurs UTCATF : forêts, sols agricoles, etc.) ou certains procédés industriels (capture et stockage ou réutilisation du carbone) et les émissions de GES sur le territoire. </p>
                    <p>Les données proviennent de diﬀérentes références bibliographiques : CITEPA, GIEC, INRA, inventaire forestier national (IGN), AGRESTE, DRAAF. </p>
                    <p>Pour plus de détail, consulter le guide méthodologique et/ou contacter l’Oreges : oreges@ligair.fr, ou Lig’Air : ligair@ligair.fr.</p>
                </div>
            </div>

            <div class="princBlock shadow-sm margin">
                <div class='sTitre'>
                    <h5>Séquestration nette de carbone</h5>
                </div>
                <div><p class="texte">En 2016, la séquestration nette de carbone du territoire est estimé à <?php echo number_format($Carbone20[8],0,'',' ')?> teqCO2. Pour rappel, les émissions de GES des grands secteurs économiques du territoire étaient de <?php echo number_format($TotGES[8],0,'',' ')?> teqCO2. La neutralité carbone <?php if ($TotDif[8] == 0){echo ' est atteinte ';}
                                                                                                                                                                                                                                                                                                                                                            else if ($TotDif < 0){echo 'est dépassée';}
                                                                                                                                                                                                                                                                                                                                                                    else { echo 'n’est pas atteinte';}   ?> puisqu’il résulte, de la somme de la séquestration nette et des émissions de GES des grands secteurs économiques, un bilan <?php if ($TotDif[8]==0) { echo 'neutre de '.number_format($TotDif[8],0,'',' ').' teqCO2. Le territoire émet autant de CO2 que ce qu’il en séquestre.';}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            elseif ($TotDif[8]>0){echo 'positif de '.number_format($TotDif[8],0,'',' ').' teqCO2. Le territoire émet plus de CO2 que ce qu’il en séquestre.';}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            else {echo 'negatif de '.number_format($TotDif[8],0,'',' ').' teqCO2. Le territoire émet moins de CO2 que ce qu’il en séquestre.';}?><p></div>
                <div class="blockRepart">
                    <div id="chartBarCarbone" class="padding m60" ></div>
                    <div id="chartPieCarbone" class="padding m40" ></div>    
                </div> 
                <div class="blockRepart">
                    <p class="texte"><U>Séquestration nette de carbone :</U> <i>(somme des ﬂux : accroissement forestier, changement d’aﬀectation des sols, défrichement et récolte du bois)</i></p>
                    <p class="texte"><U>Evaluation de la neutralité carbone :</U> <i>(sommes des émissions GES des grands secteurs économiques et de la séquestration nette)</i></p>        
                </div>   
            </div>
            <div class="princBlock shadow-sm margin "style="margin-bottom:1%;">
                <div class='sTitre'>
                    <h5>Evolution de la séquestration nette de carbone</h5>
                    </div>
                    <div class="blockRepart">
                    <div id="chartColStackCarbone" class="padding m60" ></div>
                        <div id="chartEvoCarbone" class="padding m40" ></div>
                        
                    </div>
                </div>

            

            
            <script src="../js/graphPartie2.js"></script>

        </body>
    </html>