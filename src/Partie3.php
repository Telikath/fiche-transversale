<?php

$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );

function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}
if ($_GET['Zone'] == 'EPCI'){
    $name = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "(SELECT secteur, SUM(nox_tonne) as NOX, sum(pm10_tonne)as PM10, sum(so2_tonne) as SO2, sum(benzene_kg) as C6H6, sum(hap8_kg) as HAP, sum(pm2_5_tonne) as PM2_5, sum(covnm_tonne) as COVNM, sum(nh3_tonne) as NH3	
                from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_epci ='".$_GET['ZoneBis']."'
                group by secteur)
            union (SELECT 'aaaaTotal' as secteur , SUM(nox_tonne) as NOX, sum(pm10_tonne)as PM10, sum(so2_tonne) as SO2, sum(benzene_kg) as C6H6, sum(hap8_kg) as HAP, sum(pm2_5_tonne) as PM2_5, sum(covnm_tonne) as COVNM, sum(nh3_tonne) as NH3	
                from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_epci ='".$_GET['ZoneBis']."') 
            order by secteur";
    $req2 = "SELECT annee, SUM(nox_tonne) as nox, sum(pm10_tonne)as pm10, sum(so2_tonne) as so2, sum(benzene_kg) as c6h6, sum(hap8_kg) as hap, sum(pm2_5_tonne) as pm2_5, sum(covnm_tonne) as covnm, sum(nh3_tonne) as nh3	
            from inventaire_emission.inventaire_pcaet
            WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and code_epci ='".$_GET['ZoneBis']."'
        group by annee
        order by annee ";
     $req3="SELECT max(valeur) as val, id_indicateur, annee 
     from odace.communair a inner join odace.commune c ON a.insee_com = c.code_insee  
     where code_epci ='".$_GET['ZoneBis']."'
     group by id_indicateur, annee 
     order by id_indicateur, annee";
}
else if ($_GET['Zone'] == 'Dep'){
    $name = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "(SELECT secteur, SUM(nox_tonne) as NOX, sum(pm10_tonne)as PM10, sum(so2_tonne) as SO2, sum(benzene_kg) as C6H6, sum(hap8_kg) as HAP, sum(pm2_5_tonne) as PM2_5, sum(covnm_tonne) as COVNM, sum(nh3_tonne) as NH3	
    from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_dep ='".$_GET['ZoneBis']."'
                group by secteur)
            union (SELECT 'aaaaTotal' as secteur , SUM(nox_tonne) as NOX, sum(pm10_tonne)as PM10, sum(so2_tonne) as SO2, sum(benzene_kg) as C6H6, sum(hap8_kg) as HAP, sum(pm2_5_tonne) as PM2_5, sum(covnm_tonne) as COVNM, sum(nh3_tonne) as NH3	
            from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_dep ='".$_GET['ZoneBis']."') 
            order by secteur";
    $req2 = "SELECT annee, SUM(nox_tonne) as nox, sum(pm10_tonne)as pm10, sum(so2_tonne) as so2, sum(benzene_kg) as c6h6, sum(hap8_kg) as hap, sum(pm2_5_tonne) as pm2_5, sum(covnm_tonne) as covnm, sum(nh3_tonne) as nh3	
            from inventaire_emission.inventaire_pcaet a inner join odace.commune b on a.insee_com = b.code_insee
            WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and b.code_dep ='".$_GET['ZoneBis']."'
            group by annee
            order by annee ";
    $req3="SELECT max(valeur) as val, id_indicateur, annee 
        from odace.communair a inner join odace.commune c ON a.insee_com = c.code_insee 
        where code_dep ='".$_GET['ZoneBis']."'
        group by id_indicateur, annee 
        order by id_indicateur, annee";
}
else if ($_GET['Zone'] == 'SCOT'){
    $name = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req = "(SELECT secteur, SUM(nox_tonne) as NOX, sum(pm10_tonne)as PM10, sum(so2_tonne) as SO2, sum(benzene_kg) as C6H6, sum(hap8_kg) as HAP, sum(pm2_5_tonne) as PM2_5, sum(covnm_tonne) as COVNM, sum(nh3_tonne) as NH3	
            from inventaire_emission.inventaire_pcaet a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and id_scot ='".$_GET['ZoneBis']."'
                group by secteur)
            union (SELECT 'aaaaTotal' as secteur , SUM(nox_tonne) as NOX, sum(pm10_tonne)as PM10, sum(so2_tonne) as SO2, sum(benzene_kg) as C6H6, sum(hap8_kg) as HAP, sum(pm2_5_tonne) as PM2_5, sum(covnm_tonne) as COVNM, sum(nh3_tonne) as NH3	
                from inventaire_emission.inventaire_pcaet a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and id_scot ='".$_GET['ZoneBis']."') 
            order by secteur";
    $req2 = "SELECT annee, SUM(nox_tonne) as nox, sum(pm10_tonne)as pm10, sum(so2_tonne) as so2, sum(benzene_kg) as c6h6, sum(hap8_kg) as hap, sum(pm2_5_tonne) as pm2_5, sum(covnm_tonne) as covnm, sum(nh3_tonne) as nh3	
            from inventaire_emission.inventaire_pcaet a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
            WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' and id_scot ='".$_GET['ZoneBis']."'
        group by annee
        order by annee ";
     $req3="SELECT max(valeur) as val, id_indicateur, annee 
     from odace.communair a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
     where id_scot ='".$_GET['ZoneBis']."'
     group by id_indicateur, annee 
     order by id_indicateur, annee";
}
else {
    $name = "Centre-Val de Loire";
    $req = "(SELECT secteur, SUM(nox_tonne) as NOX, sum(pm10_tonne)as PM10, sum(so2_tonne) as SO2, sum(benzene_kg) as C6H6, sum(hap8_kg) as HAP, sum(pm2_5_tonne) as PM2_5, sum(covnm_tonne) as COVNM, sum(nh3_tonne) as NH3	
                from inventaire_emission.inventaire_pcaet
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus'
                group by secteur)
            union (SELECT 'aaaaTotal' as secteur , SUM(nox_tonne) as NOX, sum(pm10_tonne)as PM10, sum(so2_tonne) as SO2, sum(benzene_kg) as C6H6, sum(hap8_kg) as HAP, sum(pm2_5_tonne) as PM2_5, sum(covnm_tonne) as COVNM, sum(nh3_tonne) as NH3	
                from inventaire_emission.inventaire_pcaet
                WHERE annee='2016' and secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus')
            order by secteur";
    $req2 = "SELECT annee, SUM(nox_tonne) as nox, sum(pm10_tonne)as pm10, sum(so2_tonne) as so2, sum(benzene_kg) as c6h6, sum(hap8_kg) as hap, sum(pm2_5_tonne) as pm2_5, sum(covnm_tonne) as covnm, sum(nh3_tonne) as nh3	
            from inventaire_emission.inventaire_pcaet
            WHERE secteur!= 'UTCATF' and secteur!= 'Emetteurs non inclus' 
            group by annee
            order by annee ";
    $req3="SELECT max(valeur) as val, id_indicateur, annee 
        from odace.communair a inner join odace.commune c ON a.insee_com = c.code_insee 
        group by id_indicateur, annee 
        order by id_indicateur, annee";
}

$LsSect = [];
$NOX = [];
$PM10 = [];
$SO2 = [];
$C6H6 = [];
$HAP = [];
$PM2_5 = [];
$COVNM = [];
$NH3 = [];
$totpoll=[];
$StatNOX=[];
$StatPM10=[];
$StatSO2=[];
$StatC6H6=[];
$StatHAP = [];
$StatPM2_5 = [];
$StatCOVNM = [];
$StatNH3 = [];



$result = donnee($monPdo, $req);
foreach($result as $row){
    if ($row['secteur'] == 'aaaaTotal'){
        $totpoll = [
            "NOX" => (float)$row['nox'],
            "PM10" => (float)$row['pm10'],
            "SO2" => (float)$row['so2'],
            "C6H6" => (float)$row['c6h6'],
            "HAP" => (float)$row['hap'],
            "PM2_5" => (float)$row['pm2_5'],
            "COVNM" => (float)$row['covnm'],
            "NH3" => (float)$row['nh3'],
           
        ];
    }
    else{
       
        array_push($LsSect, $row['secteur']);
        array_push($NOX, number_format(round($row['nox'],0,PHP_ROUND_HALF_UP),0,'',' '));
        array_push($StatNOX, round(100*$row['nox']/$totpoll['NOX'],1));
        array_push($PM10, number_format(round($row['pm10'],0,PHP_ROUND_HALF_UP),0,'',' '));
        array_push($StatPM10, round(100*$row['pm10']/$totpoll["PM10"],1));
        array_push($SO2, number_format(round($row['so2'],0,PHP_ROUND_HALF_UP),0,'',' '));
        array_push($StatSO2, round(100*$row['so2']/$totpoll["SO2"],1));
        array_push($C6H6, number_format(round($row['c6h6'],0,PHP_ROUND_HALF_UP),0,'',' '));
        array_push($StatC6H6, round(100*$row['c6h6']/$totpoll["C6H6"],1));
        array_push($HAP, number_format(round($row['hap'],0,PHP_ROUND_HALF_UP),0,'',' '));
        array_push($StatHAP, round(100*$row['hap']/$totpoll["HAP"],1));
        array_push($PM2_5, number_format(round($row['pm2_5'],0,PHP_ROUND_HALF_UP),0,'',' '));
        array_push($StatPM2_5, round(100*$row['pm2_5']/$totpoll["PM2_5"],1));
        array_push($COVNM, number_format(round($row['covnm'],0,PHP_ROUND_HALF_UP),0,'',' '));
        array_push($StatCOVNM, round(100*$row['covnm']/$totpoll["COVNM"],1));
        array_push($NH3, number_format(round($row['nh3'],0,PHP_ROUND_HALF_UP),0,'',' '));
        array_push($StatNH3, round(100*$row['nh3']/$totpoll["NH3"],1));
        
    }
    
}


echo '<script type="text/javascript">
        var dataNOX = '.json_encode($NOX).';   
        var dataStatNOX ='.json_encode($StatNOX).';
        var dataTotNOX = '.json_encode(number_format($totpoll["NOX"],0,'',' ')).'; 
        var dataSO2 = '.json_encode($SO2).';   
        var dataStatSO2 ='.json_encode($StatSO2).';
        var dataTotSO2 = '.json_encode(number_format($totpoll["SO2"],0,'',' ')).'; 
        var dataPM10 = '.json_encode($PM10).';   
        var dataStatPM10 ='.json_encode($StatPM10).';
        var dataTotPM10 = '.json_encode(number_format($totpoll["PM10"],0,'',' ')).'; 
        var dataC6H6 = '.json_encode($C6H6).';   
        var dataStatC6H6 ='.json_encode($StatC6H6).';
        var dataTotC6H6 = '.json_encode(number_format($totpoll["C6H6"],0,'',' ')).'; 
        var dataHAP = '.json_encode($HAP).';   
        var dataStatHAP ='.json_encode($StatHAP).';
        var dataTotHAP = '.json_encode(number_format($totpoll["HAP"],0,'',' ')).'; 
        var dataPM2_5 = '.json_encode($PM2_5).';   
        var dataStatPM2_5 ='.json_encode($StatPM2_5).';
        var dataTotPM2_5 = '.json_encode(number_format($totpoll["PM2_5"],0,'',' ')).'; 
        var dataCOVNM = '.json_encode($COVNM).';   
        var dataStatCOVNM ='.json_encode($StatCOVNM).';
        var dataTotCOVNM = '.json_encode(number_format($totpoll["COVNM"],0,'',' ')).'; 
        var dataNH3 = '.json_encode($NH3).';   
        var dataStatNH3 ='.json_encode($StatNH3).';
        var dataTotNH3 = '.json_encode(number_format($totpoll["NH3"],0,'',' ')).'; 
        var dataSecteur = '.json_encode($LsSect).';
        </script>';

        $evoNOX = [];
        $evoC6H6 = [];
        $evoSO2 = [];
        $evoPM10 = [];
        $evoHAP =[];
        $evoNH3 = [];
        $evoPM2_5 = [];
        $evoCOVNM = [];
        $annee =[];
        
        
        $result = donnee($monPdo, $req2);
        foreach($result as $row){
            if ($row['annee'] == '2008'){
                $totdep = [
                    "NOX" => $row['nox'],
                    "C6H6" => $row['c6h6'],
                    "SO2" => $row['so2'],
                    "PM10" => $row['pm10'],
                    "HAP" => $row['hap'],
                    "NH3" => $row['nh3'],
                    "PM2_5" => $row['pm2_5'],
                    "COVNM" => $row['covnm']
                ];
                if ($totdep['NOX'] != 0){
                    array_push($evoNOX, 100);
                }
                if ($totdep['C6H6'] != 0){
                    array_push($evoC6H6, 100);
                }
                if ($totdep['SO2'] != 0){
                    array_push($evoSO2, 100);
                }
                if ($totdep['PM10'] != 0){
                    array_push($evoPM10, 100);
                }
                if ($totdep['HAP'] != 0){
                    array_push($evoHAP, 100);
                }
                if ($totdep['NH3'] != 0){
                    array_push($evoNH3, 100);
                }
                if ($totdep['PM2_5'] != 0){
                    array_push($evoPM2_5, 100);
                }
                if ($totdep['COVNM'] != 0){
                    array_push($evoCOVNM, 100);
                }
                array_push($annee, $row['annee']);
            }
            else{
                if ($totdep['NOX'] != 0){
                    array_push($evoNOX,round(100*(($row['nox']- $totdep['NOX'])/ $totdep['NOX'])+100));
                }
                if ($totdep['C6H6'] != 0){
                    array_push($evoC6H6, round(100*(($row['c6h6']- $totdep['C6H6'])/ $totdep['C6H6'])+100));
                }
                if ($totdep['SO2'] != 0){
                    array_push($evoSO2, round(100*(($row['so2']- $totdep['SO2'])/ $totdep['SO2'])+100));
                }
                if ($totdep['PM10'] != 0){
                    array_push($evoPM10, round(100*(($row['pm10']- $totdep['PM10'])/ $totdep['PM10'])+100));
                }
                if ($totdep['HAP'] != 0){
                    array_push($evoHAP, round(100*(($row['hap']- $totdep['HAP'])/ $totdep['HAP'])+100));
                }
                if ($totdep['NH3'] != 0){
                    array_push($evoNH3, round(100*(($row['nh3']- $totdep['NH3'])/ $totdep['NH3'])+100));
                }
                if ($totdep['PM2_5'] != 0){
                    array_push($evoPM2_5, round(100*(($row['pm2_5']- $totdep['PM2_5'])/ $totdep['PM2_5'])+100));
                }
                if ($totdep['COVNM'] != 0){
                    array_push($evoCOVNM, round(100*(($row['covnm']- $totdep['COVNM'])/ $totdep['COVNM'])+100));
                }
                array_push($annee, $row['annee']);
            }
        }
        
        echo '<script type="text/javascript">
               
                var dataEvoNOX ='.json_encode($evoNOX).';
                var dataEvoC6H6 ='.json_encode($evoC6H6).';
                var dataEvoPM10 ='.json_encode($evoPM10).';
                var dataEvoSO2 ='.json_encode($evoSO2).';
                var dataEvoHAP ='.json_encode($evoHAP).';
                var dataEvoNH3 ='.json_encode($evoNH3).';
                var dataEvoPM2_5 ='.json_encode($evoPM2_5).';
                var dataEvoCOVNM ='.json_encode($evoCOVNM).';
                var annee ='.json_encode($annee).';
                </script>';
        
                   

                    $anneeBis = [];        
                    $result = donnee($monPdo, $req3);
                    foreach($result as $row){
                        if (!in_array($row['annee'], $anneeBis)){
                            array_push($anneeBis, $row['annee']);
                            ${'Comm'.$row['annee']} = [];
                        }
                            array_push(${'Comm'.$row['annee']},round($row['val'],2,PHP_ROUND_HALF_UP));
                    }
                    echo '<script type="text/javascript">
                    var Comm2013 = '.json_encode($Comm2013).';
                    var Comm2014 = '.json_encode($Comm2014).';
                    var Comm2015 = '.json_encode($Comm2015).';
                    var Comm2016 = '.json_encode($Comm2016).';
                    var Comm2017 = '.json_encode($Comm2017).';
                    var Comm2018 = '.json_encode($Comm2018).';
                    var anneeBis = '.json_encode($anneeBis).'; 
                    </script>';
                    

?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie3.css">

        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        
    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
            <div><a class="fa fa-chevron-left icon-left fa-2x valid"<?php echo "href='Partie2.php?Zone=".$_GET['Zone']."&ZoneBis=".$_GET['ZoneBis']."'"?>></a></div>
            <div class='tBan'>
                <h2>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h2>
                <h3><?php echo $name?></h3>
                <h3>Concentrations et émissions de polluants à eﬀet sanitaire (PES)</h3>
            </div>
            <div class="bt "><button class="btn btn-success"> Télécharger la fiche</button></div>
            <div><a class="fa fa-chevron-right icon-right fa-2x valid" <?php echo "href='Partie4.php?Zone=".$_GET['Zone']."&ZoneBis=".$_GET['ZoneBis']."'"?>></a></div>
            
        </div>
        <div class="princBlock margin shadow-sm">
            <div class='sTitre'>
                <h5>Émissions de polluants par secteur d’activité</h5>
            </div>
            <div><p class="texte">Les émissions de PES sur le territoire s’élèvent en 2016 à  <?php echo number_format(round($totpoll['NOX'],0,PHP_ROUND_HALF_UP),0,',',' ') ?> tonnes pour les oxydes d’azote (NOx),  <?php echo number_format(round($totpoll['PM10'],0,PHP_ROUND_HALF_UP),0,',',' ') ?> tonnes pour les particules en suspension (PM10),  <?php echo number_format(round($totpoll['SO2'],0,PHP_ROUND_HALF_UP),0,',',' ') ?> tonnes pour le dioxyde de soufre (SO2),  <?php echo number_format(round($totpoll['C6H6'],0,PHP_ROUND_HALF_UP),0,',',' ') ?> kg pour le benzène (C6H6) et  <?php echo number_format(round($totpoll['HAP'],0,PHP_ROUND_HALF_UP),0,',',' ') ?> kg pour les Hydrocarbures Aromatiques Polycycliques (HAP),  <?php echo number_format(round($totpoll['PM2_5'],0,PHP_ROUND_HALF_UP),0,',',' ') ?> tonnes pour les particules en suspension (PM2,5),  <?php echo number_format(round($totpoll['COVNM'],0,PHP_ROUND_HALF_UP),0,',',' ') ?> tonnes pour les composés organiques volatiles non méthaniques (COVNM) et  <?php echo number_format(round($totpoll['NH3'],0,PHP_ROUND_HALF_UP),0,',',' ') ?> tonnes pour l’ammoniac (NH3)</p></div>
            
            <div class='flex-row'>
                <div class="chartFlex1">
                    <div id="chartNOx"></div>
                </div>
                <div class="chartFlex2">
                    <div id="chartPM10"></div>
                </div>
                <div class="chartFlex2">
                    <div id="chartSO2"></div>
                </div>
                <div class="chartFlex2">
                    <div id="chartC6H6"></div>
                </div>
            </div>
            <div class='flex-row'>
                <div class="chartFlex1">
                    <div id="chartHAP"></div>
                </div>
                <div class="chartFlex2">
                    <div id="chartPM2_5"></div>
                </div>
                <div class="chartFlex2">
                    <div id="chartCOVNM"></div>
                </div>
                <div class="chartFlex2">
                    <div id="chartNH3"></div>
                </div>
            </div>
        </div>
        <div class="princBlock margin shadow-sm" style="margin-bottom: 0.5%">
            <div class='sTitre'>
                <h5>Evolution des émissions des Polluants à Eﬀet Sanitaire depuis 2008</h5>
            </div>
            <div><p class="texte">Les évolutions sont présentées en base 100 par rapport à l’année de référence 2008. Ainsi les émissions de PES de 2008 ont été ﬁxées à 100 % pour constater les évolutions relatives sur les années suivantes.</p></div>
            <div id="chartEvoPES" class ="padding"></div>
        </div>
    <div class="princBlock margin shadow-sm">
        <div class='sTitre'>
            <h5>Bilan de la qualité de l’air et respect de la réglementation :</h5>
        </div>
        <p class='texte'> <?php if($Comm2018[3] <= 25){
                                    echo "En situation de fond (loin des sources émettrices), aucun dépassement des valeurs limites n’a été observé sur le territoire durant l’année 2018 pour les polluants atmosphériques NO2 (dioxyde d’azote), PM10 et O3 (ozone). Malgré le respect de ces valeurs, le territoire a fait l’objet d’épisodes de pollution en PM10 conduisant aux déclenchements de procédures préféctorales d’information et recommandation mais aussi d’alerte."; if($Comm2018[4]>6000){ echo ' Seul l’objectif de qualité pour l’ozone (AOT401) a été dépassé';}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            else {echo ' l’objectif de qualité pour l’ozone (AOT401) n\'a pas été dépassé';} 
                                }
                                else if($Comm2018[3] > 25){
                                    echo "En situation de fond (loin des sources émettrices), aucun dépassement des valeurs limites n’a été observé sur le territoire durant l’année 2018 pour les polluants atmosphériques NO2 (dioxyde d’azote) et PM10. Malgré le respect de ces valeurs, le territoire a fait l’objet d’épisodes de pollution en PM10 conduisant aux déclenchements de procédures préféctorales d’information et recommandation mais aussi d’alerte."; if($Comm2018[4]>6000){ echo ' L’objectif de qualité et la valeur cible pour l’ozone (AOT40 1) ont été dépassé.';}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            else {echo ' Seul  la valeur cible pour l’ozone (AOT401) a été dépassé';} 
                                }?>.</p>
        <div class="table">
            <table class="thead-dark shadow-sm">
                <thead>
                            <tr>
                                <th>Polluants</th>
                                <th>Indicateurs</th>
                                <th>Valeurs maximales dans le territoire (Valeurs réglementaires)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>NO2</td>
                                <td>Moyenne annuelle en situation de fond</td>
                                <td><?php echo $Comm2018[0] ?> µg/m3 (valeur limite : 40 µg/m3) </td>
                                
                               
                            </tr>
                            <tr>
                                <td>PM10</td>
                                <td>Moyenne annuelle</br>Nombre de jours dépassant 50 µg/m3</td>
                                <td><?php echo $Comm2018[1] ?> µg/m3 (valeur limite : 40 µg/m3)</br><?php echo $Comm2018[2] ?> jours (valeur limite : 35 jours par an)</td>
                            </tr>
                            <tr>
                                <td>O3</td>
                                <td>Nombre de jours >120 µg/m3 en moyenne sur 8h et 3 ans</br>AOT 40</td>
                                <td><?php echo $Comm2018[3] ?> jours (valeur limite : 25 jours par an)</br><?php echo $Comm2018[4] ?> µg/m3.h (objectif qualité : 6000 µg/m3.h)</td>
                            </tr>
                        </tbody>
                    </table>
                </div> 
    </div>

    <div class="methodo shadow-sm">
                <div class="sTMeth">
                    <h5>METHODOLOGIE</h5>
                </div>
                <div class="meth">
                    <p>Le bilan de la qualité de l’air sur le territoire est basé sur la modélisation nationale (Prev’Air) ou inter-régionale (Esmeralda). Les résultats bruts issus de cette modélisation sont aﬃnés statistiquement à partir des données d’observation issues des stations ﬁxes de Lig’Air. Le bilan ne concerne que les polluants faisant l’objet de modélisation. Les PM2.5, les HAP ainsi que d’autres polluants réglementés ne sont actuellement pas modélisés.</p>
                    <p>Pour plus de précisions, vous pouvez consulter les notes méthodologiques et/ou contacter LIG’AIR : ligair@ligair.fr.</p>
                </div>
            </div>
    <div class="princBlock margin shadow-sm" style="margin-bottom:1%;">
        <div class='sTitre'>
            <h5>Evolution des indicateurs réglementaires depuis 2013</h5>
        </div>
        <div class="flex-indic">
            <div id="chartIndic1"></div>
            <div id="chartIndic2"></div>
            <div id="chartIndic3"></div>
        </div>
        <div class="flex-indic">
            <div id="chartIndic4"></div>
            <div id="chartIndic5"></div>
        </div>
    </div>
        
        <script src="../js/graphPartie3.js"></script>

    </body>
</html>