<?php
$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );



function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}

if ($_GET['Zone'] == 'EPCI'){
    $name = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="SELECT SUM(valeur) as val, a.id_scombust, annee
        FROM odace.conso a inner join odace.combustible b on a.id_combust = b.id_type and a.id_scombust = b.id_stype inner join odace.commune c ON a.insee_com = c.code_insee
        where a.id_secteur = 2 and id_scombust in (1,2,3,4,5,7,8,9) and code_epci ='".$_GET['ZoneBis']."'
        group by a.id_scombust, annee
        order by a.id_scombust, annee";

    $req2="SELECT SUM(valeur) as val, a.id_scombust, a.id_ssecteur
        FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type inner join odace.combustible c on  a.id_scombust = c.id_stype and a.id_combust = c.id_type  inner join odace.commune d ON a.insee_com = d.code_insee
        where a.id_secteur = 2 and id_scombust in (1,2,3,4,5,7,8,9) and a.id_ssecteur in (1,2,4,5,6,7,8) and code_epci ='".$_GET['ZoneBis']."'
        group by a.id_scombust, a.id_ssecteur
        order by a.id_scombust, a.id_ssecteur";
    
    $req3="SELECT SUM(valeur) as val, a.id_ssecteur as id, b.lib_stype as name, annee
    FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type  inner join odace.commune c ON a.insee_com = c.code_insee
    where a.id_secteur = 2 and a.id_ssecteur!=3 and a.id_scombust != 10 and code_epci ='".$_GET['ZoneBis']."'
    group by b.lib_stype, a.id_ssecteur, annee
    order by a.id_ssecteur, annee";

    $req4="SELECT lib_stype, sum(valeur) as val
            FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type inner join odace.commune c ON a.insee_com = c.code_insee
            where a.id_secteur = 2 and a.id_scombust != 10 and annee = 2016 and code_epci ='".$_GET['ZoneBis']."'
            group by b.lib_stype 
            ORDER BY val desc";

    function req5($resLibType){
        $req5="SELECT c.lib_stype, sum(valeur) as val
            FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type inner join odace.combustible c on  a.id_scombust = c.id_stype and a.id_combust = c.id_type inner join odace.commune d ON a.insee_com = d.code_insee  
            where a.id_secteur = 2 and b.lib_stype = '$resLibType' and a.id_scombust != 10 and annee = 2016 and code_epci ='".$_GET['ZoneBis']."'
            group by c.lib_stype 
            ORDER BY val desc";
        return $req5;
    }

    $req6="(SELECT 'Ter' as zoneter ,SUM(valeur)as val
        FROM odace.conso a inner join odace.commune c ON a.insee_com = c.code_insee
        where a.id_secteur = 2 and a.id_ssecteur in (1,2,4,5,6,7,8) and a.id_scombust != 10 and annee = 2016 and code_epci ='".$_GET['ZoneBis']."')
        union 
        (SELECT 'Reg' as zoneTer ,SUM(valeur) as val
        FROM odace.conso a  
        where a.id_secteur = 2 and a.id_ssecteur in (1,2,4,5,6,7,8) and a.id_scombust != 10 and annee = 2016)";
    
}
else if ($_GET['Zone'] == 'Dep'){
    $name = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="SELECT SUM(valeur) as val, a.id_scombust, annee
        FROM odace.conso a inner join odace.combustible b on a.id_combust = b.id_type and a.id_scombust = b.id_stype inner join odace.commune c ON a.insee_com = c.code_insee
        where a.id_secteur = 2 and id_scombust in (1,2,3,4,5,7,8,9) and code_dep ='".$_GET['ZoneBis']."'
        group by a.id_scombust, annee
        order by a.id_scombust, annee";

    $req2="SELECT SUM(valeur) as val, a.id_scombust, a.id_ssecteur
        FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type inner join odace.combustible c on  a.id_scombust = c.id_stype and a.id_combust = c.id_type  inner join odace.commune d ON a.insee_com = d.code_insee
        where a.id_secteur = 2 and id_scombust in (1,2,3,4,5,7,8,9) and a.id_ssecteur in (1,2,4,5,6,7,8) and code_dep ='".$_GET['ZoneBis']."'
        group by a.id_scombust, a.id_ssecteur
        order by a.id_scombust, a.id_ssecteur";
    
    $req3="SELECT SUM(valeur) as val, a.id_ssecteur as id, b.lib_stype as name, annee
    FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type  inner join odace.commune c ON a.insee_com = c.code_insee
    where a.id_secteur = 2 and a.id_ssecteur!=3 and a.id_scombust != 10 and code_dep ='".$_GET['ZoneBis']."'
    group by b.lib_stype, a.id_ssecteur, annee
    order by a.id_ssecteur, annee";

    $req4="SELECT lib_stype, sum(valeur) as val
            FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type inner join odace.commune c ON a.insee_com = c.code_insee
            where a.id_secteur = 2 and a.id_scombust != 10 and annee = 2016 and code_dep ='".$_GET['ZoneBis']."'
            group by b.lib_stype 
            ORDER BY val desc";

    function req5($resLibType){
        $req5="SELECT c.lib_stype, sum(valeur) as val
            FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type inner join odace.combustible c on  a.id_scombust = c.id_stype and a.id_combust = c.id_type inner join odace.commune d ON a.insee_com = d.code_insee  
            where a.id_secteur = 2 and b.lib_stype = '$resLibType' and a.id_scombust != 10 and annee = 2016 and code_dep ='".$_GET['ZoneBis']."'
            group by c.lib_stype 
            ORDER BY val desc";
        return $req5;
    }

    $req6="(SELECT 'Ter' as zoneter ,SUM(valeur)as val
        FROM odace.conso a inner join odace.commune c ON a.insee_com = c.code_insee
        where a.id_secteur = 2 and a.id_ssecteur in (1,2,4,5,6,7,8) and a.id_scombust != 10 and annee = 2016 and code_dep ='".$_GET['ZoneBis']."')
        union 
        (SELECT 'Reg' as zoneTer ,SUM(valeur) as val
        FROM odace.conso a  
        where a.id_secteur = 2 and a.id_ssecteur in (1,2,4,5,6,7,8) and a.id_scombust != 10 and annee = 2016)";
        
}
else if ($_GET['Zone'] == 'SCOT'){
    $name = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="SELECT SUM(valeur) as val, a.id_scombust, annee
        FROM odace.conso a inner join odace.combustible b on a.id_combust = b.id_type and a.id_scombust = b.id_stype inner join referentiel_geo.com_scot_2019 c on a.insee_com = c.insee_com
        where a.id_secteur = 2 and id_scombust in (1,2,3,4,5,7,8,9) and id_scot ='".$_GET['ZoneBis']."'
        group by a.id_scombust, annee
        order by a.id_scombust, annee";

    $req2="SELECT SUM(valeur) as val, a.id_scombust, a.id_ssecteur
        FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type inner join odace.combustible c on  a.id_scombust = c.id_stype and a.id_combust = c.id_type  inner join referentiel_geo.com_scot_2019 d on a.insee_com = d.insee_com
        where a.id_secteur = 2 and id_scombust in (1,2,3,4,5,7,8,9) and a.id_ssecteur in (1,2,4,5,6,7,8) and id_scot ='".$_GET['ZoneBis']."'
        group by a.id_scombust, a.id_ssecteur
        order by a.id_scombust, a.id_ssecteur";
    
    $req3="SELECT SUM(valeur) as val, a.id_ssecteur as id, b.lib_stype as name, annee
    FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type  inner join referentiel_geo.com_scot_2019 c on a.insee_com = c.insee_com
    where a.id_secteur = 2 and a.id_ssecteur!=3 and a.id_scombust != 10 and id_scot ='".$_GET['ZoneBis']."'
    group by b.lib_stype, a.id_ssecteur, annee
    order by a.id_ssecteur, annee";

    $req4="SELECT lib_stype, sum(valeur) as val
            FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type inner join referentiel_geo.com_scot_2019 c on a.insee_com = c.insee_com
            where a.id_secteur = 2 and a.id_scombust != 10 and annee = 2016 and id_scot ='".$_GET['ZoneBis']."'
            group by b.lib_stype 
            ORDER BY val desc";

    function req5($resLibType){
        $req5="SELECT c.lib_stype, sum(valeur) as val
            FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type inner join odace.combustible c on  a.id_scombust = c.id_stype and a.id_combust = c.id_type inner join referentiel_geo.com_scot_2019 d on a.insee_com = d.insee_com
            where a.id_secteur = 2 and b.lib_stype = '$resLibType' and a.id_scombust != 10 and annee = 2016 and id_scot ='".$_GET['ZoneBis']."'
            group by c.lib_stype 
            ORDER BY val desc";
        return $req5;
    }

    $req6="(SELECT 'Ter' as zoneter ,SUM(valeur)as val
        FROM odace.conso a inner join referentiel_geo.com_scot_2019 b on a.insee_com = b.insee_com
        where a.id_secteur = 2 and a.id_ssecteur in (1,2,4,5,6,7,8) and a.id_scombust != 10 and annee = 2016 and id_scot ='".$_GET['ZoneBis']."')
        union 
        (SELECT 'Reg' as zoneTer ,SUM(valeur) as val
        FROM odace.conso a  
        where a.id_secteur = 2 and a.id_ssecteur in (1,2,4,5,6,7,8) and a.id_scombust != 10 and annee = 2016)";
    
}
else {
    $name = "Centre-Val de Loire";
    $req="SELECT SUM(valeur) as val, a.id_scombust, annee
        FROM odace.conso a inner join odace.combustible b on a.id_combust = b.id_type and a.id_scombust = b.id_stype inner join odace.commune c ON a.insee_com = c.code_insee
        where a.id_secteur = 2 and id_scombust in (1,2,3,4,5,7,8,9)
        group by a.id_scombust, annee
        order by a.id_scombust, annee";

    $req2="SELECT SUM(valeur) as val, a.id_scombust, a.id_ssecteur
        FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type inner join odace.combustible c on  a.id_scombust = c.id_stype and a.id_combust = c.id_type  inner join odace.commune d ON a.insee_com = d.code_insee
        where a.id_secteur = 2 and id_scombust in (1,2,3,4,5,7,8,9) and a.id_ssecteur in (1,2,4,5,6,7,8)
        group by a.id_scombust, a.id_ssecteur
        order by a.id_scombust, a.id_ssecteur";
    
    $req3="SELECT SUM(valeur) as val, a.id_ssecteur as id, b.lib_stype as name, annee
    FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type  inner join odace.commune c ON a.insee_com = c.code_insee
    where a.id_secteur = 2 and a.id_ssecteur!=3 and a.id_scombust != 10 
    group by b.lib_stype, a.id_ssecteur, annee
    order by a.id_ssecteur, annee";

    $req4="SELECT lib_stype, sum(valeur) as val
            FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type 
            where a.id_secteur = 2 and a.id_scombust != 10 and annee = 2016
            group by b.lib_stype 
            ORDER BY val desc";
    
    function req5($resLibType){
    $req5="SELECT c.lib_stype, sum(valeur) as val
        FROM odace.conso a inner join odace.secteur b on a.id_ssecteur = b.id_stype and a.id_secteur = b.id_type inner join odace.combustible c on  a.id_scombust = c.id_stype and a.id_combust = c.id_type inner join odace.commune d ON a.insee_com = d.code_insee  
        where a.id_secteur = 2 and b.lib_stype = '$resLibType' and a.id_scombust != 10 and annee = 2016
        group by c.lib_stype 
        ORDER BY val desc";
    return $req5;
    }

    $req6="(SELECT 'Ter' as zoneter ,SUM(valeur)as val
        FROM odace.conso a inner join odace.commune c ON a.insee_com = c.code_insee
        where a.id_secteur = 2 and a.id_ssecteur in (1,2,4,5,6,7,8) and a.id_scombust != 10 and annee = 2016)
        union 
        (SELECT 'Reg' as zoneTer ,SUM(valeur) as val
        FROM odace.conso a  
        where a.id_secteur = 2 and a.id_ssecteur in (1,2,4,5,6,7,8) and a.id_scombust != 10 and annee = 2016)";
    
}

$LsSect = [];



$i="";
$totCombGlb = "";
$annee = [];

$result = donnee($monPdo, $req);
    foreach($result as $row){
        if ($i != $row['id_scombust']){
            $i = $row['id_scombust'];
            ${'totComb'.$i} = [];
            
            $totCombGlb = $row['val'];
            if($totCombGlb != 0){
                array_push(${'totComb'.$i}, 100);
            }
            
        }
        else {
            if($totCombGlb != 0){
                array_push(${'totComb'.$i}, round(100*(($row['val']- $totCombGlb)/ $totCombGlb)+100) );
            }
           
        }
        if (!in_array($row['annee'], $annee)){
            array_push($annee, $row['annee']);
        }
    }
    echo '<script type="text/javascript">
        var totComb1 = '.json_encode($totComb1).';
        var totComb2 = '.json_encode($totComb2).';
        var totComb3 = '.json_encode($totComb3).';
        var totComb4 = '.json_encode($totComb4).';
        var totComb5 = '.json_encode($totComb5).';
        var totComb7 = '.json_encode($totComb7).';
        var totComb8 = '.json_encode($totComb8).';
        var totComb9 = '.json_encode($totComb9).';
        var annee = '.json_encode($annee).'; 
        </script>';

$i="";

$result = donnee($monPdo, $req2);
    foreach($result as $row){
        if ($i != $row['id_scombust']){
            $i = $row['id_scombust'];
            ${'totSectType'.$i} = [];
            array_push(${'totSectType'.$i}, round($row['val'],0,PHP_ROUND_HALF_UP));
        }
        else {
            array_push(${'totSectType'.$i}, round($row['val'],0,PHP_ROUND_HALF_UP));
        }
    }
    echo '<script type="text/javascript">
        var totSectType1 = '.json_encode($totSectType1).';
        var totSectType2 = '.json_encode($totSectType2).';
        var totSectType3 = '.json_encode($totSectType3).';
        var totSectType4 = '.json_encode($totSectType4).';
        var totSectType5 = '.json_encode($totSectType5).';
        var totSectType7 = '.json_encode($totSectType7).';
        var totSectType8 = '.json_encode($totSectType8).';
        var totSectType9 = '.json_encode($totSectType9).';
        </script>';



$result = donnee($monPdo, $req3);
$i = 0;
$lsTotS = [];
foreach($result as $row){
    if ($i != $row['id']){
        $i = $row['id'];
        ${'lsTotS'.$i} = [];
        array_push(${'lsTotS'.$i}, round($row[0],0,PHP_ROUND_HALF_UP));
        if ($row['annee'] == 2016){
            array_push($lsTotS, round($row[0],0,PHP_ROUND_HALF_UP));
        }
    }
    else {
        if ($row['annee'] == 2016){
            array_push($lsTotS, round($row[0],0,PHP_ROUND_HALF_UP));
        }
        array_push(${'lsTotS'.$i}, round($row[0],0,PHP_ROUND_HALF_UP));

    }
    if (!in_array($row['name'], $LsSect)){
        array_push($LsSect, $row['name']);
    }
}
echo '<script type="text/javascript">
    var lsTotS = '.json_encode($lsTotS).';
    var lsTotS1 = '.json_encode($lsTotS1).';
    var lsTotS2 = '.json_encode($lsTotS2).';
    var lsTotS4 = '.json_encode($lsTotS4).';
    var lsTotS5 = '.json_encode($lsTotS5).';
    var lsTotS6 = '.json_encode($lsTotS6).';
    var lsTotS7 = '.json_encode($lsTotS7).';
    var lsTotS8 = '.json_encode($lsTotS8).';
    var dataSecteur = '.json_encode($LsSect).';      

</script>';

        # texte

        $resLib = donnee($monPdo, $req4);
        $resLibType = $resLib[0]['lib_stype'];
        
        $resLib = donnee($monPdo, req5($resLibType)); 
        $resLibComb = $resLib[0]['lib_stype'];

        
    
        $result = donnee($monPdo, $req6);
            foreach($result as $row){
                ${'Tot2016'.$row['zoneter']} = $row['val'];
            }

?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie4.css">

    
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        
    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
            <div><a class="fa fa-chevron-left icon-left fa-2x valid" <?php echo "href='Partie3.php?Zone=".$_GET['Zone']."&ZoneBis=".$_GET['ZoneBis']."'"?>></a></div>
            <div class='tBan'>
                <h2>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h2>
                <h3><?php echo $name?></h3>
                <h3>Consommation d’énergie ﬁnale</h3>
            </div>
            <div class="bt "><button class="btn btn-success"> Télécharger la fiche</button></div>
            <div><a class="fa fa-chevron-right icon-right fa-2x valid" <?php echo "href='Partie5.php?Zone=".$_GET['Zone']."&ZoneBis=".$_GET['ZoneBis']."'"?>></a></div>
            
        </div>
        <div><p class="texte">En 2016, environ <?php echo number_format(round($Tot2016Ter),0,'',' ')?> tep ont été consommées sur le territoire (soit environ <?php echo round(100*round($Tot2016Ter)/round($Tot2016Reg),1,PHP_ROUND_HALF_UP) ?>% de la consommation d’énergie ﬁnale en région Centre-Val de Loire). Le secteur <?php echo $resLibType?> est le principal poste de consommation énergétique sur le territoire. Les <?php echo $resLibComb?> constituent l’énergie la plus consommée dans ce secteur.</p></div>
            <div class="blockMethRep">
            <div class="princBlock margin shadow-sm m70">
                <div class='sTitre'>
                    <h5>Répartition de la consommation d’énergie ﬁnale par secteur et par type</h5>
                </div>
                <div id="chartBarFin" class="padding"></div>
            </div>
            <div class="methodo shadow-sm">
                <div class="sTMeth">
                    <h5>METHODOLOGIE</h5>
                </div>
                <div class="meth">
                    <p>La donnée de consommation d’énergie ﬁnale est issue du travail d’inventaire des émissions de polluants atmosphériques réalisé par Lig’Air. Conformément au PCAET, « la consommation énergétique ﬁnale est ainsi la consommation de toutes les branches de l’économie, à l’exception des quantités consommées par les producteurs et transformateurs d’énergie (exemple : consommation propre d’une raﬃnerie) et des quantités de produits énergétiques transformés en d’autres produits ». Concernant le secteur des déchets et conformément à cette déﬁnition seules sont prises en compte dans l’Atlas les consommations des unités de traitement qui ne valorisent pas l’énergie. Autrement dit Les consommations du secteur Branche énergie ne sont pas comptabilisées. Aﬁn de prendre en compte l’ensemble des énergies, l’électricité et la chaleur sont ajoutées aux combustibles (utilisés à des ﬁns de consommation énergétique) évalués dans l’inventaire des émissions. Les données sont fournies à climat réel, c’est-à-dire qu’elles ne sont pas corrigées des variations climatiques et le pouvoir caloriﬁque inférieur pour les combustibles est retenu conformément à l’arrêté PCAET.</p>
                    <p>La tonne d’équivalent pétrole (tep) est une unité de mesure couramment utilisée pour comparer les diﬀérentes énergies entre-elles. C’est l’énergie produite par la combustion d’une tonne de pétrole moyen (1 tep=11,6 MWh). Pour plus de précisions, vous pouvez consulter les notes méthodologiques et/ou contacter l’OREGES : oreges@ligair.fr.</p>
                </div>
            </div>
        </div>
        <div class="blockRepart ">
            <div class="princBlock margin shadow-sm m50">
                <div class='sTitre'>
                    <h5>Répartition de la consommation d’énergie ﬁnale par secteur et par type</h5>
                </div>
                <div id="chartLineFin" class="padding"></div>
            </div>
        
            <div class="princBlock margin shadow-sm m50" style="margin-left: 0;">
                <div class='sTitre'>
                    <h5>Répartition de la consommation d’énergie ﬁnale par secteur</h5>
                </div>
                <div id="chartPieFin" class="padding"></div>
            </div>
         </div>       
            <div class="princBlock margin shadow-sm" style="margin-bottom: 0.5%">
                <div class='sTitre'>
                    <h5>Evolution de la consommation d’energie ﬁnale par secteurs entre 2008 et 2016</h5>
                </div>
                <div id="chartColStackFin" class="padding"></div>
            </div>
            
        
        <script src="../js/graphPartie4.js"></script>

    </body>
</html>