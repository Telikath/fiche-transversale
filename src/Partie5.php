<?php
$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );


function donnee($monPdo, $req){
    $res=$monPdo->query($req); 
    $result = $res->fetchAll ();
    $res -> closeCursor();
    return $result;
}


if ($_GET['Zone'] == 'EPCI'){
    $name = "SELECT code_epci from odace.epci where nom_epci='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="SELECT sum(a.valeur) as val, b.lib_annee as annee
        FROM odace.zoom_res  a inner join odace.commune c ON a.insee_com = c.code_insee inner join odace.zoom_annee b on a.id_annee = b.id_annee
        WHERE a.id_cat = 5 and a.id_combust = 12 and code_epci ='".$_GET['ZoneBis']."'
        group by b.id_annee
        order by b.id_annee";
    
    $req2="SELECT sum(a.valeur) as val, a.id_combust 
        FROM odace.zoom_res a  inner join odace.commune c ON a.insee_com = c.code_insee
        WHERE a.id_cat = 5 and a.id_combust in (8,9, 10, 11) and code_epci ='".$_GET['ZoneBis']."'
        group by a.id_combust
        order by a.id_combust";

    $req3="SELECT sum(valeur)
        FROM odace.zoom_res a inner join odace.commune c ON a.insee_com = c.code_insee
        WHERE id_cat = 5 and id_combust = 7 and code_epci ='".$_GET['ZoneBis']."'";
    $req6="SELECT sum(a.valeur) as val, a.id_annee
        FROM odace.zoom_res a inner join odace.commune c ON a.insee_com = c.code_insee
        WHERE a.id_cat = 4 and a.id_combust in (1,2,3,4,5,6) and code_epci ='".$_GET['ZoneBis']."'
        group by a.id_annee
        order by a.id_annee"; 
    $req7="SELECT sum(valeur)
        FROM odace.zoom_res a inner join odace.commune c ON a.insee_com = c.code_insee
        WHERE id_cat = 4 and code_epci ='".$_GET['ZoneBis']."'";
    $req8="SELECT SUM(valeur) as val, b.lib_zoom_type as type , a.id_annee
    FROM odace.zoom_res a inner join odace.zoom_type b on a.id_combust = b.id_zoom_type inner join odace.commune c ON a.insee_com = c.code_insee
    where a.id_cat= 4 and code_epci ='".$_GET['ZoneBis']."'
    group by b.lib_zoom_type, a.id_annee 
    order by b.lib_zoom_type desc, a.id_annee";
}
else if ($_GET['Zone'] == 'Dep'){
    $name = "SELECT depname from odace.departement where depnumber='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="SELECT sum(a.valeur) as val, b.lib_annee as annee
        FROM odace.zoom_res  a inner join odace.commune c ON a.insee_com = c.code_insee inner join odace.zoom_annee b on a.id_annee = b.id_annee
        WHERE a.id_cat = 5 and a.id_combust = 12 and code_dep ='".$_GET['ZoneBis']."'
        group by b.id_annee
        order by b.id_annee";
    
    $req2="SELECT sum(a.valeur) as val, a.id_combust 
        FROM odace.zoom_res a  inner join odace.commune c ON a.insee_com = c.code_insee
        WHERE a.id_cat = 5 and a.id_combust in (8,9, 10, 11) and code_dep ='".$_GET['ZoneBis']."'
        group by a.id_combust
        order by a.id_combust";

    $req3="SELECT sum(valeur)
        FROM odace.zoom_res a inner join odace.commune c ON a.insee_com = c.code_insee
        WHERE id_cat = 5 and id_combust = 7 and code_dep ='".$_GET['ZoneBis']."'";
    $req6="SELECT sum(a.valeur) as val, a.id_annee
            FROM odace.zoom_res a inner join odace.commune c ON a.insee_com = c.code_insee
            WHERE a.id_cat = 4 and a.id_combust in (1,2,3,4,5,6) and code_dep ='".$_GET['ZoneBis']."'
            group by a.id_annee
            order by a.id_annee"; 
    $req7="SELECT sum(valeur)
            FROM odace.zoom_res a inner join odace.commune c ON a.insee_com = c.code_insee
            WHERE id_cat = 4 and code_dep ='".$_GET['ZoneBis']."'"; 
    $req8="SELECT SUM(valeur) as val, b.lib_zoom_type as type , a.id_annee
            FROM odace.zoom_res a inner join odace.zoom_type b on a.id_combust = b.id_zoom_type inner join odace.commune c ON a.insee_com = c.code_insee
            where a.id_cat= 4 and code_dep ='".$_GET['ZoneBis']."'
            group by b.lib_zoom_type, a.id_annee 
            order by b.lib_zoom_type desc, a.id_annee";
}
else if ($_GET['Zone'] == 'SCOT'){
    $name = "SELECT nom_scot as nom from referentiel_geo.com_scot_2019 where id_scot='".$_GET['ZoneBis']."'";
    $name = donnee($monPdo, $name)[0][0];
    $req="SELECT sum(a.valeur) as val, b.lib_annee as annee
        FROM odace.zoom_res  a inner join referentiel_geo.com_scot_2019 c on a.insee_com = c.insee_com inner join odace.zoom_annee b on a.id_annee = b.id_annee
        WHERE a.id_cat = 5 and a.id_combust = 12 and id_scot ='".$_GET['ZoneBis']."'
        group by b.id_annee
        order by b.id_annee";
    
    $req2="SELECT sum(a.valeur) as val, a.id_combust 
        FROM odace.zoom_res a  inner join referentiel_geo.com_scot_2019 c on a.insee_com = c.insee_com
        WHERE a.id_cat = 5 and a.id_combust in (8,9, 10, 11) and id_scot ='".$_GET['ZoneBis']."'
        group by a.id_combust
        order by a.id_combust";

    $req3="SELECT sum(valeur)
        FROM odace.zoom_res a inner join referentiel_geo.com_scot_2019 c on a.insee_com = c.insee_com
        WHERE id_cat = 5 and id_combust = 7 and id_scot ='".$_GET['ZoneBis']."'";
    $req6="SELECT sum(a.valeur) as val, a.id_annee
        FROM odace.zoom_res a inner join referentiel_geo.com_scot_2019 c on a.insee_com = c.insee_com
        WHERE a.id_cat = 4 and a.id_combust in (1,2,3,4,5,6) and id_scot ='".$_GET['ZoneBis']."'
        group by a.id_annee
        order by a.id_annee"; 
    $req7="SELECT sum(valeur)
        FROM odace.zoom_res a inner join referentiel_geo.com_scot_2019 c on a.insee_com = c.insee_com
        WHERE id_cat = 4 and id_scot ='".$_GET['ZoneBis']."'";
    $req8="SELECT SUM(valeur) as val, b.lib_zoom_type as type , a.id_annee
    FROM odace.zoom_res a inner join odace.zoom_type b on a.id_combust = b.id_zoom_type inner join referentiel_geo.com_scot_2019 c on a.insee_com = c.insee_com
    where a.id_cat= 4 and id_scot ='".$_GET['ZoneBis']."'
    group by b.lib_zoom_type, a.id_annee 
    order by b.lib_zoom_type desc, a.id_annee";
}
else {
    $name = "Centre-Val de Loire";
    $req="SELECT sum(a.valeur) as val, b.lib_annee as annee
        FROM odace.zoom_res  a inner join odace.commune c ON a.insee_com = c.code_insee inner join odace.zoom_annee b on a.id_annee = b.id_annee
        WHERE a.id_cat = 5 and a.id_combust = 12
        group by b.id_annee
        order by b.id_annee";
    
    $req2="SELECT sum(a.valeur) as val, a.id_combust 
        FROM odace.zoom_res a  inner join odace.commune c ON a.insee_com = c.code_insee
        WHERE a.id_cat = 5 and a.id_combust in (8,9, 10, 11)
        group by a.id_combust
        order by a.id_combust";

    $req3="SELECT sum(valeur)
        FROM odace.zoom_res a inner join odace.commune c ON a.insee_com = c.code_insee
        WHERE id_cat = 5 and id_combust = 7";
    $req6="SELECT sum(a.valeur) as val, a.id_annee
    FROM odace.zoom_res a inner join odace.commune c ON a.insee_com = c.code_insee
    WHERE a.id_cat = 4 and a.id_combust in (1,2,3,4,5,6)
    group by a.id_annee
    order by a.id_annee"; 
$req7="SELECT sum(valeur)
    FROM odace.zoom_res a inner join odace.commune c ON a.insee_com = c.code_insee
    WHERE id_cat = 4";   
    $req8="SELECT SUM(valeur) as val, b.lib_zoom_type as type , a.id_annee
    FROM odace.zoom_res a inner join odace.zoom_type b on a.id_combust = b.id_zoom_type inner join odace.commune c ON a.insee_com = c.code_insee
    where a.id_cat= 4 
    group by b.lib_zoom_type, a.id_annee 
    order by b.lib_zoom_type desc, a.id_annee";
    

}
$LsZoomTot = [];

$result = donnee($monPdo, $req);
    foreach($result as $row){
        array_push($LsZoomTot,round($row['val'],0,PHP_ROUND_HALF_UP));
    }

echo '<script type="text/javascript">
    var LsZoomTot = '.json_encode($LsZoomTot).';
    </script>';



$LsZoomData = [];

    
$result = donnee($monPdo, $req2);
    foreach($result as $row){
        array_push($LsZoomData,round($row['val'],0,PHP_ROUND_HALF_UP));
    }

echo '<script type="text/javascript">
    var LsZoomData = '.json_encode($LsZoomData).';
    </script>';


    $result = donnee($monPdo, $req3);
   
        $totSURF = round($result[0][0],0,PHP_ROUND_HALF_UP);
    
    $LsZoomDataReg = []; 

    $req4="SELECT sum(a.valeur) as val, a.id_combust 
        FROM odace.zoom_res a  inner join odace.commune c ON a.insee_com = c.code_insee
        WHERE a.id_cat = 5 and a.id_combust in (8,9, 10, 11)
        group by a.id_combust
        order by a.id_combust";
    $result = donnee($monPdo, $req4);
    foreach($result as $row){
        array_push($LsZoomDataReg,round($row['val'],0,PHP_ROUND_HALF_UP));
    }

    $req5="SELECT sum(valeur)
        FROM odace.zoom_res a inner join odace.commune c ON a.insee_com = c.code_insee
        WHERE id_cat = 5 and id_combust = 7";
   $result = donnee($monPdo, $req5);
   
        $totSURFReg = round($result[0][0],0,PHP_ROUND_HALF_UP);

        $result = donnee($monPdo, $req8);
        $combCourant = "";
        $lsCombZoom = [];
            foreach($result as $row){
                    if ($combCourant != $row['type']){
                        $combCourant = $row['type'];
                        ${'LsZoom'.$combCourant} = [];
                        array_push(${'LsZoom'.$combCourant},round($row['val'],0,PHP_ROUND_HALF_UP));
                        array_push($lsCombZoom, $row['type']);
                    }
                    else {
                        array_push(${'LsZoom'.$combCourant},round($row['val'],0,PHP_ROUND_HALF_UP));
                    }
                        
                }
        foreach ($lsCombZoom as $comb){
            echo '<script type="text/javascript">
            var lsComb'.$comb.' = '.json_encode(${'LsZoom'.$comb}).';
            </script>';
        }
        echo '<script type="text/javascript">
        var lsCombZoom = '.json_encode($lsCombZoom).';
        </script>';

        $resultByA = donnee($monPdo, $req6);


$result = donnee($monPdo, $req7);
    $totCONSO = round($result[0][0],0,PHP_ROUND_HALF_UP);
    
?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partie5.css">

        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        
    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
            <div><a class="fa fa-chevron-left icon-left fa-2x valid" <?php echo "href='Partie4.php?Zone=".$_GET['Zone']."&ZoneBis=".$_GET['ZoneBis']."'"?>></a></div>
            <div class='tBan'>
                <h2>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h2>
                <h3><?php echo $name?></h3>
                <h3>Zoom secteur résidentiel : parc de logements et utilisation du chauﬀage</h3>
            </div>
            <div class="bt "><button class="btn btn-success"> Télécharger la fiche</button></div>
            <div><a class="fa fa-chevron-right icon-right fa-2x valid" <?php echo "href='Partie6.php?Zone=".$_GET['Zone']."&ZoneBis=".$_GET['ZoneBis']."'"?>></a></div>
            
        </div>
        <p class="texte" style="margin-top: 1%;">La description du parc de logements en 2016 sur le territoire est issue de l’INSEE et de la base Sit@del2 (base des permis de construire).</p>
        <div class="blockRepart ">
            <div class="princBlock margin shadow-sm m70">
                <div class='sTitre'>
                    <h5>Répartition de la surface des logements par année de construction</h5>
                </div>
                <div id="chartBarZoom1" class="padding"></div>
            </div>
            <div class="margin m15 blockMethRep" style=" margin-left:0;">
            <p class="texte" style="margin-top: 1%;">Sur le territoire, la surface totale de logements est de <?php echo number_format(round($totSURF,0,PHP_ROUND_HALF_UP),0,'',' ') ?>m2. </br><?php echo round(100*($LsZoomTot[0]+$LsZoomTot[1]+$LsZoomTot[2])/$totSURF,0,PHP_ROUND_HALF_UP).' %' ?> des logements (en surface) ont été construits avant 1970, soit avant toute réglementation thermique</p>
                <p class="texte" style="margin-top: 1%;">Au niveau régional, la part de résidences principales est de <?php echo round(100*$LsZoomDataReg[0]/$totSURFReg,0,PHP_ROUND_HALF_UP) ?> % et celle du logement individuel est de <?php echo round(100*$LsZoomDataReg[2]/$totSURFReg,0,PHP_ROUND_HALF_UP) ?> %.</p>
            </div>
        </div>
        <div class="blockRepart ">
            <div class="princBlock margin shadow-sm m50">
                <div class='sTitre'>
                    <h5>Répartition de la surface des logements par type de résidences (principales/secondaires)</h5>
                </div>
                <div id="chartBarZoom2" class="padding"></div>
            </div>
            <div class="princBlock margin shadow-sm m50" style="margin-left:0;">
                <div class='sTitre'>
                    <h5>Répartition de la surface des logements par type (collectif/individuel)</h5>
                </div>
                <div id="chartBarZoom3" class="padding"></div>
            </div>
        </div>
            <div class="princBlock margin shadow-sm ">
                <div class='sTitre'>
                    <h5>Répartition des consommations d’énergie ﬁnale pour les besoins du chauﬀage par type d’énergie en fonction du parc de logement</h5>
                </div>
                <div id="chartBarZoom4" class="padding"></div>
            </div>
            <div class="princBlock margin shadow-sm" style="margin-bottom: 0.5%">
                <div class='sTitre'>
                    <h5>Répartition des surfaces de logements et des consommations d’énergie ﬁnale pour les besoins du chauﬀage par année de construction</h5>
                </div>
                <div class="table">
                    <table class="thead-dark shadow-sm">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Avant 1919</th>
                                <th>De 1919 à 1945</th>
                                <th>De 1946 a 1970</th>
                                <th>De 1971 à 1991</th>
                                <th>De 1991 à 2005</th>
                                <th>Après 2006</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Surfaces des logements</td>
                                <td><?php echo round(100*$LsZoomTot[0]/$totSURF,0,PHP_ROUND_HALF_UP).' %' ?></td>
                                <td><?php echo round(100*$LsZoomTot[1]/$totSURF,0,PHP_ROUND_HALF_UP).' %' ?></td>
                                <td><?php echo round(100*$LsZoomTot[2]/$totSURF,0,PHP_ROUND_HALF_UP).' %' ?></td>
                                <td><?php echo round(100*$LsZoomTot[3]/$totSURF,0,PHP_ROUND_HALF_UP).' %' ?></td>
                                <td><?php echo round(100*$LsZoomTot[4]/$totSURF,0,PHP_ROUND_HALF_UP).' %' ?></td>
                                <td><?php echo round(100*$LsZoomTot[5]/$totSURF,0,PHP_ROUND_HALF_UP).' %' ?></td>
                            </tr>
                            <tr>
                                <td>Consommations liées au chauffage</td>
                                <td><?php echo round(100*$resultByA[0]['val']/$totCONSO,0,PHP_ROUND_HALF_UP).' %' ?></td>
                                <td><?php echo round(100*$resultByA[1]['val']/$totCONSO,0,PHP_ROUND_HALF_UP).' %' ?></td>
                                <td><?php echo round(100*$resultByA[2]['val']/$totCONSO,0,PHP_ROUND_HALF_UP).' %' ?></td>
                                <td><?php echo round(100*$resultByA[3]['val']/$totCONSO,0,PHP_ROUND_HALF_UP).' %' ?></td>
                                <td><?php echo round(100*$resultByA[4]['val']/$totCONSO,0,PHP_ROUND_HALF_UP).' %' ?></td>
                                <td><?php echo round(100*$resultByA[5]['val']/$totCONSO,0,PHP_ROUND_HALF_UP).' %' ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div> 
            </div>
        <script src="../js/graphPartie5.js"></script>

    </body>
</html>