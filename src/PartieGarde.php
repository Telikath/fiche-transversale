<?php 

$monPdo = new PDO ('pgsql:host=ligair.fr;dbname=ligair','ligair','8wwupawqub' );
$monPdo->query ( "SET CHARACTER SET utf8" );


$req="SELECT nom_epci as code, code_epci as nom from odace.epci ";
$res=$monPdo->query($req); 
$resultSect = $res->fetchAll ();
$res -> closeCursor();
echo '<script type="text/javascript">
    var AllEPCI = '.json_encode($resultSect).';    

    </script>';

$req="SELECT distinct(id_scot) as code, nom_scot as nom from referentiel_geo.com_scot_2019";
    $res=$monPdo->query($req); 
    $resultScot = $res->fetchAll ();
    $res -> closeCursor();
    echo '<script type="text/javascript">
        var AllSCOT = '.json_encode($resultScot).';    
    
        </script>';

?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

        <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/partieGarde.css">

    </head>
    <body>
        <div class="shadow-sm p-0 mb-0  titre">
            <div class='tBan'>
                <h1>ATLAS TRANSVERSAL CLIMAT-AIR-ENERGIE</h1>
                <h2>Territoire de la région Centre-Val de Loire</h2>
            </div>
        </div>
            <form action="Partie0.php" method="get">
                <div class="donnee">
                    <p>
                    Cette ﬁche territoriale synthétise les principales informations relatives aux émissions de Gaz à Eﬀet de Serre (GES), 
                    à la qualité de l’air, à la séquestration carbone, à la consommation d’énergie et à la production d’énergie renouvelable à l’échelle de la région. 
                    Ces informations sont principalement issues de l’inventaire des émissions atmosphériques réalisé par Lig’Air pour l’année de référence 2016. 
                    À la ﬁn de ce document sont fournis des informations et des liens vers des données complémentaires à cette ﬁche synthétique.
                    </p>
                    <div class="flax"> 
                    <label for="selectZone" >Choisisser votre echelle ></label>
                        <select name="Zone" id="selectZone" onchange="alimente();" class="form-control">
                            <option value="Reg">Region</option>
                            <option value="Dep">Département</option>
                            <option value="EPCI">EPCI</option>
                            <option value="SCOT">SCOT</option>
                        </select>
                    <label for="selectZoneBis" id="labelZone" ></label>
                        <select name="ZoneBis" id="selectZoneBis" class="form-control"></select>
                        
                    </div>
                    <div class="flax">
                        <button type="submit" class="btn btn-outline-success">OK</button>
                    <div>
                </div>
            </form>   
     
    
    
    </body>
</html>

<script>
    
function alimente(){

    if (document.getElementById('selectZone').value == "Reg"){
        document.getElementById('selectZoneBis').style.display = "none";
        document.getElementById('selectZoneBis').options.length = 0;
        document.getElementById('selectZoneBis').options[document.getElementById('selectZoneBis').options.length]=new Option('0','0');
        document.getElementById('labelZone').innerHTML="";
    }
	else if (document.getElementById('selectZone').value == "Dep")
	{
        document.getElementById('labelZone').innerHTML="Choisisser un département >"
	    document.getElementById('selectZoneBis').style.display = "inline";
		document.getElementById('selectZoneBis').options.length = 0;
		document.getElementById('selectZoneBis').options[document.getElementById('selectZoneBis').options.length]=new Option('Cher','18');
		document.getElementById('selectZoneBis').options[document.getElementById('selectZoneBis').options.length]=new Option('Eure-et-Loir','28');
		document.getElementById('selectZoneBis').options[document.getElementById('selectZoneBis').options.length]=new Option('Indre','36');
        document.getElementById('selectZoneBis').options[document.getElementById('selectZoneBis').options.length]=new Option('Indre-et-Loire','37');
        document.getElementById('selectZoneBis').options[document.getElementById('selectZoneBis').options.length]=new Option('Loir-et-Cher','41');
        document.getElementById('selectZoneBis').options[document.getElementById('selectZoneBis').options.length]=new Option('Loiret','45');
	}
	else if (document.getElementById('selectZone').value == "EPCI")
	{
        document.getElementById('labelZone').innerHTML="Choisisser une EPCI >"
	    document.getElementById('selectZoneBis').style.display = "inline";
        document.getElementById('selectZoneBis').options.length = 0;
    
        for (var i = 0; i < AllEPCI.length; i++) {
            document.getElementById('selectZoneBis').options[document.getElementById('selectZoneBis').options.length]=new Option(AllEPCI[i]['nom'],AllEPCI[i]['code']);
        }
    }
    else if (document.getElementById('selectZone').value == "SCOT"){
        document.getElementById('labelZone').innerHTML="Choisisser un SCOT >"
	    document.getElementById('selectZoneBis').style.display = "inline";
        document.getElementById('selectZoneBis').options.length = 0;
    
        for (var i = 0; i < AllSCOT.length; i++) {
            document.getElementById('selectZoneBis').options[document.getElementById('selectZoneBis').options.length]=new Option(AllSCOT[i]['nom'],AllSCOT[i]['code']);
        }
        
	}
	
	
	
}

alimente();

        
</script>